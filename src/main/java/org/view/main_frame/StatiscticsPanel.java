package org.view.main_frame;

import org.model.data_files.CECHY_DRUGORZEDNE;
import org.model.data_files.CECHY_GLOWNE;

import javax.swing.*;
import java.awt.*;

public class StatiscticsPanel {
    private JPanel panel;
    private StatiscticsLabel mainProfileHorizontal;
    private Statisctic.MainProfileView startingMainProfile;
    private Statisctic.MainProfileView differentialMainProfile;
    private Statisctic.MainProfileView finalMainProfile;

    private StatiscticsLabel secondProfileHorizontal;
    private Statisctic.SecondProfileView startingSecondProfile;
    private Statisctic.SecondProfileView differentialSecondProfile;
    private Statisctic.SecondProfileView finalSecondProfile;

    public StatiscticsPanel() {
        // Set up Panel
        panel = new JPanel();
        panel.setLayout(new GridLayout(8,1));

        // Create UI elements
        mainProfileHorizontal = new StatiscticsLabel(
                new String[]{
                        "",
                        CECHY_GLOWNE.WALKA_WRECZ,
                        CECHY_GLOWNE.UMIEJETNOSCI_STRZELECKIE,
                        CECHY_GLOWNE.KRZEPA,
                        CECHY_GLOWNE.ODPORNOSC,
                        CECHY_GLOWNE.ZRECZNOSC,
                        CECHY_GLOWNE.INTELIGENCJA,
                        CECHY_GLOWNE.SILA_WOLI,
                        CECHY_GLOWNE.OGLADA},
                1,
                9
        );

        startingMainProfile = new Statisctic.MainProfileView("Cechy Początkowe",false);
        differentialMainProfile = new Statisctic.MainProfileView("Schemat",true);
        finalMainProfile = new Statisctic.MainProfileView("Aktualne",false);

        secondProfileHorizontal = new StatiscticsLabel(
                new String[]{
                        "",
                        CECHY_DRUGORZEDNE.ATAKI,
                        CECHY_DRUGORZEDNE.ZYWOTNOSC,
                        CECHY_DRUGORZEDNE.SILA,
                        CECHY_DRUGORZEDNE.WYTRZYMALOSC,
                        CECHY_DRUGORZEDNE.SZYBKOSC,
                        CECHY_DRUGORZEDNE.MAGIA,
                        CECHY_DRUGORZEDNE.PUNKTY_OBLEDU,
                        CECHY_DRUGORZEDNE.PUNKTY_PRZEZNACZENIA},
                1,
                9
        );

        startingSecondProfile = new Statisctic.SecondProfileView("Cechy Początkowe",false);
        differentialSecondProfile = new Statisctic.SecondProfileView("Schemat",true);
        finalSecondProfile = new Statisctic.SecondProfileView("Aktualne",false);

        // Add UI elements to panel
        panel.add(mainProfileHorizontal);
        panel.add(startingMainProfile);
        panel.add(differentialMainProfile);
        panel.add(finalMainProfile);

        panel.add(secondProfileHorizontal);
        panel.add(startingSecondProfile);
        panel.add(differentialSecondProfile);
        panel.add(finalSecondProfile);
    }


    private class StatiscticsLabel extends JComponent {

        StatiscticsLabel(String[] labelNames, int nrows, int ncols) {
            // Set up layout
            setLayout(new GridLayout(nrows, ncols));

            for (String labelName : labelNames) {
                add(new JLabel(labelName));
            }
        }
    }

    public JPanel getPanel() {
        return panel;
    }

    public Statisctic.MainProfileView getStartingMainProfile() {
        return startingMainProfile;
    }

    public Statisctic.MainProfileView getDifferentialMainProfile() {
        return differentialMainProfile;
    }

    public Statisctic.MainProfileView getFinalMainProfile() {
        return finalMainProfile;
    }

    public Statisctic.SecondProfileView getStartingSecondProfile() {
        return startingSecondProfile;
    }

    public Statisctic.SecondProfileView getDifferentialSecondProfile() {
        return differentialSecondProfile;
    }

    public Statisctic.SecondProfileView getFinalSecondProfile() {
        return finalSecondProfile;
    }
}
