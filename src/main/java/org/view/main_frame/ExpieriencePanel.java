package org.view.main_frame;

import javax.swing.*;
import java.awt.*;

public class ExpieriencePanel {
    private JPanel panel;

    private JTextField expToAdd;

    private JLabel expToUseLabel;
    private JLabel expUsedLabel;

    private JButton addExpButton;
    private JButton expToUse;
    private JButton expUsed;

    ExpieriencePanel() {
        // Create UI elements
        panel = new JPanel();
        panel.setLayout(new GridLayout(0,3));

        expToAdd = new JTextField();
        expToUseLabel = new JLabel("PD");
        expUsedLabel = new JLabel("Wykorzystane PD");
        addExpButton = new JButton("Dodaj PD");
        expToUse = new JButton();
        expUsed = new JButton();

        expToUse.setEnabled(false);
        expUsed.setEnabled(false);

        panel.add(expToAdd);
        panel.add(expToUseLabel);
        panel.add(expUsedLabel);
        panel.add(addExpButton);
        panel.add(expToUse);
        panel.add(expUsed);
    }

    public JPanel getPanel() {
        return panel;
    }

    public JTextField getExpToAdd() {
        return expToAdd;
    }

    public JButton getAddExpButton() {
        return addExpButton;
    }

    public JButton getExpToUse() {
        return expToUse;
    }

    public JButton getExpUsed() {
        return expUsed;
    }
}
