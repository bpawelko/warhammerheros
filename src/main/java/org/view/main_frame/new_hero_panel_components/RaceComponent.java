package org.view.main_frame.new_hero_panel_components;

import org.model.data_files.RASY;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class RaceComponent {
    private JPanel racePanel;
    private ButtonGroup raceGroup;
    private Border etchedBorder;

    public RaceComponent() {
        // Setting up panel
        racePanel = new JPanel();
        racePanel.setLayout(new GridLayout(2,2));
        etchedBorder = BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Rasa");
        racePanel.setBorder(etchedBorder);

        // Create UI elements
        raceGroup = new ButtonGroup();

        // Add UI elements to panel
        racePanel.add(addRadioButton(RASY.CZLOWIEK));
        racePanel.add(addRadioButton(RASY.ELF));
        racePanel.add(addRadioButton(RASY.KRASNOLUD));
        racePanel.add(addRadioButton(RASY.NIZOLEK));
    }

    private JRadioButton addRadioButton(String title) {
        JRadioButton button = new JRadioButton(title);
        raceGroup.add(button);
        return button;
    }

    public JPanel getRacePanel() {
        return racePanel;
    }

    public ButtonGroup getRaceGroup() {
        return raceGroup;
    }
}
