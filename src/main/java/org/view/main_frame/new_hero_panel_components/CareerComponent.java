package org.view.main_frame.new_hero_panel_components;

import org.model.data_files.PROFESJE;
import org.model.data_files.RASY;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class CareerComponent {
    private JPanel careerPanel;
    private ButtonGroup careerGroup;
    private Border etchedBorder;

    public CareerComponent() {
        // Setting up panel
        careerPanel = new JPanel();
        careerPanel.setLayout(new GridLayout(2,2));
        etchedBorder = BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Profesja");
        careerPanel.setBorder(etchedBorder);

        // Create UI elements
        careerGroup = new ButtonGroup();

        // Add UI elements to panel
        careerPanel.add(addRadioButton(PROFESJE.AKOLITA));
        careerPanel.add(addRadioButton(PROFESJE.BANITA));
        careerPanel.add(addRadioButton(PROFESJE.CHLOP));
    }

    private JRadioButton addRadioButton(String title) {
        JRadioButton button = new JRadioButton(title);
        careerGroup.add(button);
        return button;
    }

    public JPanel getCareerPanel() {
        return careerPanel;
    }

    public ButtonGroup getCareerGroup() {
        return careerGroup;
    }
}
