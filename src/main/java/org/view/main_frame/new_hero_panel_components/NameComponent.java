package org.view.main_frame.new_hero_panel_components;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class NameComponent {
    private JPanel namePanel;
    private JLabel nameLabel;
    private JTextField nameTextField;
    private Border etchedBorder;


    public NameComponent() {
        // Setting up panel
        namePanel = new JPanel();
        namePanel.setLayout(new GridLayout(1,0));

        etchedBorder = BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Podstawowe Informacje");
        namePanel.setBorder(etchedBorder);

        // Create UI elements
        nameLabel = new JLabel("Imię Bohatera");
        nameTextField = new JTextField();

        // Add UI elements to panel
        namePanel.add(nameLabel);
        namePanel.add(nameTextField);
    }

    public JPanel getNamePanel() {
        return namePanel;
    }

    public JTextField getNameTextField() {
        return nameTextField;
    }
}
