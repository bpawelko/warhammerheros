package org.view.main_frame;

import org.view.main_frame.new_hero_panel_components.CareerComponent;
import org.view.main_frame.new_hero_panel_components.NameComponent;
import org.view.main_frame.new_hero_panel_components.RaceComponent;

import javax.swing.*;
import java.awt.*;

public class NewHeroPanel {
    private JPanel panel;
    private NameComponent nameComponent;
    private RaceComponent raceComponent;
    private CareerComponent careerComponent;
    private JButton makeHeroButton;

    public NewHeroPanel() {
        // Setting up panel
        panel = new JPanel();
        panel.setLayout(new BorderLayout());

        // Create UI elements
        makeHeroButton = new JButton("Stwórz");
        nameComponent = new NameComponent();
        raceComponent = new RaceComponent();
        careerComponent = new CareerComponent();

        // Add UI elements to panel
        panel.add(nameComponent.getNamePanel(), BorderLayout.NORTH);
        panel.add(raceComponent.getRacePanel(), BorderLayout.CENTER);
        panel.add(careerComponent.getCareerPanel(), BorderLayout.SOUTH);
        panel.add(makeHeroButton, BorderLayout.EAST);
    }

    public NameComponent getNameComponent() {
        return nameComponent;
    }

    public RaceComponent getRaceComponent() {
        return raceComponent;
    }

    public CareerComponent getCareerComponent() {
        return careerComponent;
    }

    public JButton getMakeHeroButton() {
        return makeHeroButton;
    }

    public JPanel getPanel() {
        return panel;
    }
}
