package org.view.main_frame;

import org.model.data_files.CECHY_DRUGORZEDNE;
import org.model.data_files.CECHY_GLOWNE;

import javax.swing.*;
import java.awt.*;

public class Statisctic {

    public static class MainProfileView extends JComponent {
        private JLabel label;
        private JButton waeponSkill;
        private JButton ballisticSkill;
        private JButton strength;
        private JButton toughness;
        private JButton agility;
        private JButton intelligence;
        private JButton willPower;
        private JButton fellowship;

        public MainProfileView(String labelName, boolean enabledButton) {
            // Set up Layout
            setLayout(new GridLayout(1,9));

            // Create UI elements
            label = new JLabel(labelName);
            waeponSkill = new JButton(CECHY_GLOWNE.WALKA_WRECZ);
            ballisticSkill = new JButton(CECHY_GLOWNE.UMIEJETNOSCI_STRZELECKIE);
            strength = new JButton(CECHY_GLOWNE.KRZEPA);
            toughness = new JButton(CECHY_GLOWNE.ODPORNOSC);
            agility = new JButton(CECHY_GLOWNE.ZRECZNOSC);
            intelligence = new JButton(CECHY_GLOWNE.INTELIGENCJA);
            willPower = new JButton(CECHY_GLOWNE.SILA_WOLI);
            fellowship = new JButton(CECHY_GLOWNE.OGLADA);

            waeponSkill.setEnabled(enabledButton);
            ballisticSkill.setEnabled(enabledButton);
            strength.setEnabled(enabledButton);
            toughness.setEnabled(enabledButton);
            agility.setEnabled(enabledButton);
            intelligence.setEnabled(enabledButton);
            willPower.setEnabled(enabledButton);
            fellowship.setEnabled(enabledButton);

            // Add UI elements
            add(label);
            add(waeponSkill);
            add(ballisticSkill);
            add(strength);
            add(toughness);
            add(agility);
            add(intelligence);
            add(willPower);
            add(fellowship);
        }

        public JButton getWaeponSkill() {
            return waeponSkill;
        }

        public JButton getBallisticSkill() {
            return ballisticSkill;
        }

        public JButton getStrength() {
            return strength;
        }

        public JButton getToughness() {
            return toughness;
        }

        public JButton getAgility() {
            return agility;
        }

        public JButton getIntelligence() {
            return intelligence;
        }

        public JButton getWillPower() {
            return willPower;
        }

        public JButton getFellowship() {
            return fellowship;
        }
    }

    public static class SecondProfileView extends JComponent {
        private JLabel label;
        private JButton attacks;
        private JButton wounds;
        private JButton strengthBonus;
        private JButton toughnessBonus;
        private JButton movement;
        private JButton magic;
        private JButton insanityPoints;
        private JButton fatePoints;

        public SecondProfileView(String labelName, boolean enabledButton) {
            // Set up Layout
            setLayout(new GridLayout(1,9));

            // Create UI elements
            label = new JLabel(labelName);
            attacks = new JButton(CECHY_DRUGORZEDNE.ATAKI);
            wounds = new JButton(CECHY_DRUGORZEDNE.ZYWOTNOSC);
            strengthBonus = new JButton(CECHY_DRUGORZEDNE.SILA);
            toughnessBonus = new JButton(CECHY_DRUGORZEDNE.WYTRZYMALOSC);
            movement = new JButton(CECHY_DRUGORZEDNE.SZYBKOSC);
            magic = new JButton(CECHY_DRUGORZEDNE.MAGIA);
            insanityPoints = new JButton(CECHY_DRUGORZEDNE.PUNKTY_OBLEDU);
            fatePoints = new JButton(CECHY_DRUGORZEDNE.PUNKTY_PRZEZNACZENIA);

            attacks.setEnabled(enabledButton);
            wounds.setEnabled(enabledButton);
            strengthBonus.setEnabled(enabledButton);
            toughnessBonus.setEnabled(enabledButton);
            movement.setEnabled(enabledButton);
            magic.setEnabled(enabledButton);
            insanityPoints.setEnabled(enabledButton);
            fatePoints.setEnabled(enabledButton);

            // Add UI elements
            add(label);
            add(attacks);
            add(wounds);
            add(strengthBonus);
            add(toughnessBonus);
            add(movement);
            add(magic);
            add(insanityPoints);
            add(fatePoints);
        }

        public JButton getAttacks() {
            return attacks;
        }

        public JButton getWounds() {
            return wounds;
        }

        public JButton getStrengthBonus() {
            return strengthBonus;
        }

        public JButton getToughnessBonus() {
            return toughnessBonus;
        }

        public JButton getMovement() {
            return movement;
        }

        public JButton getMagic() {
            return magic;
        }

        public JButton getInsanityPoints() {
            return insanityPoints;
        }

        public JButton getFatePoints() {
            return fatePoints;
        }
    }
}
