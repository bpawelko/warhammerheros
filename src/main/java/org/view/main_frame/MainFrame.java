package org.view.main_frame;

import org.view.data_files.ICONS_PATHS;
import org.view.data_files.main_frame.MAIN_FRAME;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainFrame {
    private JFrame mainFrame;
    private JMenuBar menuBar;
    private JMenu menuNewFile;
    private JMenuItem menuNewFileNewHero;
    private JMenuItem menuNewFileExit;

    private JTextArea textArea;
    private JScrollPane scrollPane;

    private StatiscticsPanel statiscticsPanel;
    private ExpieriencePanel expieriencePanel;

    public MainFrame() {
        // Setting up main JFrame
        mainFrame = new JFrame(MAIN_FRAME.APP_TITLE);
        
        int screenWidth = Toolkit.getDefaultToolkit().getScreenSize().width;
        int screenHeight = Toolkit.getDefaultToolkit().getScreenSize().height;
        mainFrame.setSize(screenWidth/2,screenHeight/2);

        int frameWidth = mainFrame.getWidth();
        int frameHeight = mainFrame.getHeight();
        mainFrame.setLocation((screenWidth - frameWidth)/2, (screenHeight - frameHeight)/2);

        mainFrame.setIconImage(Toolkit.getDefaultToolkit().getImage(ICONS_PATHS.EMPIRE_SYMBOL_1));
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainFrame.setVisible(true);

        // Create UI elements
            // MenuBar
        menuBar = new JMenuBar();
        menuNewFile = new JMenu(MAIN_FRAME.MENU_NEW_FILE);
        menuNewFileNewHero = new JMenuItem(MAIN_FRAME.MENU_NEW_FILE_NEW_HERO);
        menuNewFileExit = new JMenuItem(MAIN_FRAME.MENU_NEW_FILE_EXIT);

        menuNewFile.add(menuNewFileNewHero);
        menuNewFile.add(menuNewFileExit);
        menuBar.add(menuNewFile);

            // Text area
        textArea = new JTextArea(40,20);
        scrollPane = new JScrollPane(
                textArea,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS
        );
            // Statistics panel
        statiscticsPanel = new StatiscticsPanel();
        expieriencePanel = new ExpieriencePanel();

        // Add UI elements to frame
        mainFrame.add(menuBar, BorderLayout.NORTH);
        mainFrame.add(scrollPane, BorderLayout.EAST);
        mainFrame.add(statiscticsPanel.getPanel(), BorderLayout.CENTER);
        mainFrame.add(expieriencePanel.getPanel(), BorderLayout.SOUTH);
    }

    public JFrame getMainFrame() {
        return mainFrame;
    }

    public JMenuItem getMenuNewFileNewHero() {
        return menuNewFileNewHero;
    }

    public JMenuItem getMenuNewFileExit() {
        return menuNewFileExit;
    }

    public JTextArea getTextArea() {
        return textArea;
    }

    public StatiscticsPanel getStatiscticsPanel() {
        return statiscticsPanel;
    }

    public ExpieriencePanel getExpieriencePanel() {
        return expieriencePanel;
    }
}
