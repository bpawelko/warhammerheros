package org.view.data_files.main_frame;

public class MAIN_FRAME {
    public static final String APP_TITLE = "WarhammerHeroes";
    public static final String MENU_NEW_FILE = "Plik";
    public static final String MENU_NEW_FILE_NEW_HERO = "Nowy bohater";
    public static final String MENU_NEW_FILE_EXIT = "Wyjście";
    public static final String HERO_LABEL_INFO = "Imię bohatera";
}
