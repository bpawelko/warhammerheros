package org.controller.main_frame;

import org.model.cechy.Cecha;
import org.model.cechy.CechyGlowne;
import org.view.main_frame.Statisctic;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StatisticMainProfileController {
    private Statisctic.MainProfileView mainProfileView;
    private CechyGlowne mainProfile;

    StatisticMainProfileController(Statisctic.MainProfileView mainProfileView, CechyGlowne mainProfile) {
        this.mainProfileView = mainProfileView;
        this.mainProfile = mainProfile;

        mainProfileView.getWaeponSkill().setText(Integer.toString(mainProfile.getWalkaWrecz().getWartosc()));
        mainProfileView.getBallisticSkill().setText(Integer.toString(mainProfile.getUmiejetnosciStrzeleckie().getWartosc()));
        mainProfileView.getStrength().setText(Integer.toString(mainProfile.getKrzepa().getWartosc()));
        mainProfileView.getToughness().setText(Integer.toString(mainProfile.getOdpornosc().getWartosc()));
        mainProfileView.getAgility().setText(Integer.toString(mainProfile.getZrecznosc().getWartosc()));
        mainProfileView.getIntelligence().setText(Integer.toString(mainProfile.getInteligencja().getWartosc()));
        mainProfileView.getWillPower().setText(Integer.toString(mainProfile.getSilaWoli().getWartosc()));
        mainProfileView.getFellowship().setText(Integer.toString(mainProfile.getOglada().getWartosc()));
    }

    public void update() {
        mainProfileView.getWaeponSkill().setText(Integer.toString( mainProfile.getWalkaWrecz().getWartosc()));
        mainProfileView.getBallisticSkill().setText(Integer.toString(mainProfile.getUmiejetnosciStrzeleckie().getWartosc()));
        mainProfileView.getStrength().setText(Integer.toString(mainProfile.getKrzepa().getWartosc()));
        mainProfileView.getToughness().setText(Integer.toString(mainProfile.getOdpornosc().getWartosc()));
        mainProfileView.getAgility().setText(Integer.toString(mainProfile.getZrecznosc().getWartosc()));
        mainProfileView.getIntelligence().setText(Integer.toString(mainProfile.getInteligencja().getWartosc()));
        mainProfileView.getWillPower().setText(Integer.toString(mainProfile.getSilaWoli().getWartosc()));
        mainProfileView.getFellowship().setText(Integer.toString(mainProfile.getOglada().getWartosc()));
    }

    public Statisctic.MainProfileView getMainProfileView() {
        return mainProfileView;
    }
}
