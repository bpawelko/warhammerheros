package org.controller.main_frame;

import org.controller.main_frame.new_hero_panel_components.CareerComponentController;
import org.controller.main_frame.new_hero_panel_components.RaceComponentController;
import org.model.postacie.postac_gracza.PostacGracza;
import org.view.main_frame.MainFrame;
import org.view.main_frame.NewHeroPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

public class NewHeroPanelController {
    private String heroName;
    private PostacGracza hero;
    private NewHeroPanel newHeroPanel;
    private JDialog newHeroDialog;

    NewHeroPanelController(NewHeroPanel newHeroPanel, JTextArea textArea) {
        this.newHeroPanel = newHeroPanel;

        CareerComponentController careerController =
                new CareerComponentController(newHeroPanel.getCareerComponent(), textArea);
        RaceComponentController raceController =
                new RaceComponentController(newHeroPanel.getRaceComponent(), textArea);

        // Add Action Listeners
        newHeroPanel.getMakeHeroButton().addActionListener(
                new MakeHeroListener(raceController, careerController, textArea)
        );
    }

    private class MakeHeroListener implements ActionListener {
        private CareerComponentController careerController;
        private RaceComponentController raceController;
        private JTextArea textArea;

        MakeHeroListener(RaceComponentController raceController,
                         CareerComponentController careerController,
                         JTextArea textArea) {
            this.raceController = raceController;
            this.careerController = careerController;
            this.textArea = textArea;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            heroName = newHeroPanel.getNameComponent().getNameTextField().getText();
            try {
                hero = new PostacGracza(
                        heroName,
                        raceController.getChosenRace().getDeclaredConstructor().newInstance(),
                        careerController.getChosenCareer().getDeclaredConstructor().newInstance());
                textArea.insert("Utworzono Bohatera\n",0);
                newHeroDialog.dispose();
            }
            catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
                ex.printStackTrace();
            }
            System.out.println(hero);
        }
    }

    public void showNewHeroDialog(Frame parent, String title) {
        Frame owner = null;

        if (parent instanceof Frame)
            owner = (Frame) parent;
        else
            owner = (Frame) SwingUtilities.getAncestorOfClass(Frame.class,parent);

        if (newHeroDialog == null || newHeroDialog.getOwner() != owner) {
            newHeroDialog = new JDialog(owner, true);
            newHeroDialog.add(newHeroPanel.getPanel());
            newHeroDialog.pack();
        }

        newHeroDialog.setTitle(title);
        newHeroDialog.setVisible(true);
    }

    public NewHeroPanel getNewHeroPanel() {
        return newHeroPanel;
    }

    public PostacGracza getHero() {
        return hero;
    }

    public JDialog getNewHeroDialog() {
        return newHeroDialog;
    }
}
