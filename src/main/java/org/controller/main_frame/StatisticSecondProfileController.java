package org.controller.main_frame;

import org.model.cechy.CechyDrugorzedne;
import org.model.cechy.CechyGlowne;
import org.view.main_frame.Statisctic;

public class StatisticSecondProfileController {
    private Statisctic.SecondProfileView secondProfileView;
    private CechyDrugorzedne secondProfile;

    StatisticSecondProfileController(Statisctic.SecondProfileView  secondProfileView, CechyDrugorzedne secondProfile) {
        this.secondProfileView = secondProfileView;
        this.secondProfile = secondProfile;

        secondProfileView.getAttacks().setText(Integer.toString(secondProfile.getAtaki().getWartosc()));
        secondProfileView.getWounds().setText(Integer.toString(secondProfile.getZywotnosc().getWartosc()));
        secondProfileView.getStrengthBonus().setText(Integer.toString(secondProfile.getSila().getWartosc()));
        secondProfileView.getToughnessBonus().setText(Integer.toString(secondProfile.getWytrzymalosc().getWartosc()));
        secondProfileView.getMovement().setText(Integer.toString(secondProfile.getSzybkosc().getWartosc()));
        secondProfileView.getMagic().setText(Integer.toString(secondProfile.getMagia().getWartosc()));
        secondProfileView.getInsanityPoints().setText(Integer.toString(secondProfile.getPunktyObledu().getWartosc()));
        secondProfileView.getFatePoints().setText(Integer.toString(secondProfile.getPunktyPrzeznaczenia().getWartosc()));
    }

    public void update() {
        secondProfileView.getAttacks().setText(Integer.toString(secondProfile.getAtaki().getWartosc()));
        secondProfileView.getWounds().setText(Integer.toString(secondProfile.getZywotnosc().getWartosc()));
        secondProfileView.getStrengthBonus().setText(Integer.toString(secondProfile.getSila().getWartosc()));
        secondProfileView.getToughnessBonus().setText(Integer.toString(secondProfile.getWytrzymalosc().getWartosc()));
        secondProfileView.getMovement().setText(Integer.toString(secondProfile.getSzybkosc().getWartosc()));
        secondProfileView.getMagic().setText(Integer.toString(secondProfile.getMagia().getWartosc()));
        secondProfileView.getInsanityPoints().setText(Integer.toString(secondProfile.getPunktyObledu().getWartosc()));
        secondProfileView.getFatePoints().setText(Integer.toString(secondProfile.getPunktyPrzeznaczenia().getWartosc()));
    }

    public Statisctic.SecondProfileView getSecondProfileView() {
        return secondProfileView;
    }
}
