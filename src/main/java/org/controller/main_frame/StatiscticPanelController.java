package org.controller.main_frame;

import org.model.cechy.Cecha;
import org.model.data_files.CECHY_DRUGORZEDNE;
import org.model.data_files.CECHY_GLOWNE;
import org.model.postacie.postac_gracza.PostacGracza;
import org.model.postacie.postac_gracza.UlepszenieCech;
import org.model.postacie.postac_gracza.statystyki.CechySpecjalne;
import org.view.main_frame.ExpieriencePanel;
import org.view.main_frame.StatiscticsPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StatiscticPanelController {
    private StatisticMainProfileController startingMainProfile;
    private StatisticMainProfileController differentialMainProfile;
    private StatisticMainProfileController finalMainProfile;

    private StatisticSecondProfileController startingSecondProfile;
    private StatisticSecondProfileController differentialSecondProfile;
    private StatisticSecondProfileController finalSecondProfile;
    private ExpieriencePanel expieriencePanel;
    private JTextArea jTextArea;

    StatiscticPanelController(
            PostacGracza hero,
            StatiscticsPanel statiscticsPanel,
            ExpieriencePanel expieriencePanel,
            JTextArea jTextArea
    ) {

        this.expieriencePanel = expieriencePanel;
        this.jTextArea = jTextArea;

        startingMainProfile = new StatisticMainProfileController(
                statiscticsPanel.getStartingMainProfile(),
                hero.getCechySpecjalne().getCechyGlowneAktualne());

        differentialMainProfile = new StatisticMainProfileController(
                statiscticsPanel.getDifferentialMainProfile(),
                hero.getCechySpecjalne().getCechyGlowneSchematRoznicowy());

        finalMainProfile = new StatisticMainProfileController(
                statiscticsPanel.getFinalMainProfile(),
                hero.getCechySpecjalne().getCechyGlowneAktualne());

        startingSecondProfile = new StatisticSecondProfileController(
                statiscticsPanel.getStartingSecondProfile(),
                hero.getCechySpecjalne().getCechyDrugorzednePoczatkowe());

        differentialSecondProfile = new StatisticSecondProfileController(
                statiscticsPanel.getDifferentialSecondProfile(),
                hero.getCechySpecjalne().getCechyDrugorzedneSchematRoznicowy());

        finalSecondProfile = new StatisticSecondProfileController(statiscticsPanel.getFinalSecondProfile(),
                hero.getCechySpecjalne().getCechyDrugorzedneAktualne());


        differentialMainProfile.getMainProfileView().getWaeponSkill().addActionListener(
                new upgradeMainProfileListener(hero, CECHY_GLOWNE.WALKA_WRECZ));
        differentialMainProfile.getMainProfileView().getBallisticSkill().addActionListener(
                new upgradeMainProfileListener(hero, CECHY_GLOWNE.UMIEJETNOSCI_STRZELECKIE));
        differentialMainProfile.getMainProfileView().getStrength().addActionListener(
                new upgradeMainProfileListener(hero, CECHY_GLOWNE.KRZEPA));
        differentialMainProfile.getMainProfileView().getToughness().addActionListener(
                new upgradeMainProfileListener(hero, CECHY_GLOWNE.ODPORNOSC));
        differentialMainProfile.getMainProfileView().getAgility().addActionListener(
                new upgradeMainProfileListener(hero, CECHY_GLOWNE.ZRECZNOSC));
        differentialMainProfile.getMainProfileView().getIntelligence().addActionListener(
                new upgradeMainProfileListener(hero, CECHY_GLOWNE.INTELIGENCJA));
        differentialMainProfile.getMainProfileView().getWillPower().addActionListener(
                new upgradeMainProfileListener(hero, CECHY_GLOWNE.SILA_WOLI));
        differentialMainProfile.getMainProfileView().getFellowship().addActionListener(
                new upgradeMainProfileListener(hero, CECHY_GLOWNE.OGLADA));

        differentialSecondProfile.getSecondProfileView().getAttacks().addActionListener(
                new upgradeSecondProfileListener(hero, CECHY_DRUGORZEDNE.ATAKI));
        differentialSecondProfile.getSecondProfileView().getWounds().addActionListener(
                new upgradeSecondProfileListener(hero, CECHY_DRUGORZEDNE.ZYWOTNOSC));
        differentialSecondProfile.getSecondProfileView().getStrengthBonus().addActionListener(
                new upgradeSecondProfileListener(hero, CECHY_DRUGORZEDNE.SILA));
        differentialSecondProfile.getSecondProfileView().getToughnessBonus().addActionListener(
                new upgradeSecondProfileListener(hero, CECHY_DRUGORZEDNE.WYTRZYMALOSC));
        differentialSecondProfile.getSecondProfileView().getMovement().addActionListener(
                new upgradeSecondProfileListener(hero, CECHY_DRUGORZEDNE.SZYBKOSC));
        differentialSecondProfile.getSecondProfileView().getMagic().addActionListener(
                new upgradeSecondProfileListener(hero, CECHY_DRUGORZEDNE.MAGIA));
        differentialSecondProfile.getSecondProfileView().getInsanityPoints().addActionListener(
                new upgradeSecondProfileListener(hero, CECHY_DRUGORZEDNE.PUNKTY_OBLEDU));
        differentialSecondProfile.getSecondProfileView().getFatePoints().addActionListener(
                new upgradeSecondProfileListener(hero, CECHY_DRUGORZEDNE.PUNKTY_PRZEZNACZENIA));

    }

    private class upgradeMainProfileListener implements ActionListener {
        private PostacGracza hero;
        private String name;

        upgradeMainProfileListener(PostacGracza hero, String name) {
            this.hero = hero;
            this.name = name;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                UlepszenieCech.UlepszCecheGlowna(hero, name);
                jTextArea.insert("Ulepszono: " + name + "\n", 0);
                differentialMainProfile.update();
                finalMainProfile.update();
                finalSecondProfile.update();
                expieriencePanel.getExpToUse().setText(Integer.toString(hero.getPunktyDoswiadczenia().getPunktyDoswiadczeniaObecne()));
                expieriencePanel.getExpUsed().setText(Integer.toString(hero.getPunktyDoswiadczenia().getPunktyDoswiadczeniaRazem()));
            } catch (Exception ex) {
                jTextArea.insert(ex.getMessage() + "\n",0);
            }
        }
    }

    private class upgradeSecondProfileListener implements ActionListener {
        private PostacGracza hero;
        private String name;

        upgradeSecondProfileListener(PostacGracza hero, String name)
        {
            this.hero = hero;
            this.name = name;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                UlepszenieCech.UlepszCecheDrugorzedna(hero, name);
                jTextArea.insert("Ulepszono: " + name, 0);
                differentialSecondProfile.update();
                finalSecondProfile.update();
                expieriencePanel.getExpToUse().setText(Integer.toString(hero.getPunktyDoswiadczenia().getPunktyDoswiadczeniaObecne()));
                expieriencePanel.getExpUsed().setText(Integer.toString(hero.getPunktyDoswiadczenia().getPunktyDoswiadczeniaRazem()));
            } catch (Exception ex) {
                jTextArea.insert(ex.getMessage() + "\n",0);
            }
        }
    }
}
