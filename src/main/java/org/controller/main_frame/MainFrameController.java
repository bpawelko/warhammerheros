package org.controller.main_frame;

import org.model.postacie.postac_gracza.PostacGracza;
import org.view.main_frame.MainFrame;
import org.view.main_frame.NewHeroPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MainFrameController {
    // TODO initlize w kontrolerach
    private StatiscticPanelController statiscticPanelController;
    private ExpieriencePanelController expieriencePanelController;
    private NewHeroPanelController newHeroPanelController;

    public MainFrameController() {
        final MainFrame mainFrame = new MainFrame();

        // Controllers

        // Action Listeners
            // MenuBar Listeners
        mainFrame.getMenuNewFileNewHero().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newHeroPanelController = new NewHeroPanelController(
                        new NewHeroPanel(),
                        mainFrame.getTextArea()
                );

                newHeroPanelController.showNewHeroDialog(mainFrame.getMainFrame(),"Stwórz nowego bohatera");
                newHeroPanelController.getNewHeroDialog().addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosed(WindowEvent e) {
                        super.windowClosed(e);
                        statiscticPanelController = new StatiscticPanelController(
                                newHeroPanelController.getHero(),
                                mainFrame.getStatiscticsPanel(),
                                mainFrame.getExpieriencePanel(),
                                mainFrame.getTextArea());
                        expieriencePanelController = new ExpieriencePanelController(
                                mainFrame.getExpieriencePanel(),
                                newHeroPanelController.getHero().getPunktyDoswiadczenia(),
                                mainFrame.getTextArea());
                    }
                });
            }
        });

        mainFrame.getMenuNewFileExit().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
    }
}
