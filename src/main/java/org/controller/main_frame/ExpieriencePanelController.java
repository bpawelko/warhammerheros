package org.controller.main_frame;

import org.model.postacie.postac_gracza.statystyki.PunktyDoswiadczenia;
import org.view.main_frame.ExpieriencePanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ExpieriencePanelController {
    private PunktyDoswiadczenia punktyDoswiadczenia;
    private ExpieriencePanel expieriencePanel;
    private JTextArea jTextArea;


    ExpieriencePanelController(
            ExpieriencePanel expieriencePanel, PunktyDoswiadczenia punktyDoswiadczenia, JTextArea jTextArea) {

        this.punktyDoswiadczenia = punktyDoswiadczenia;
        this.expieriencePanel = expieriencePanel;
        this.jTextArea = jTextArea;

        expieriencePanel.getExpToUse().setText(Integer.toString(punktyDoswiadczenia.getPunktyDoswiadczeniaObecne()));
        expieriencePanel.getExpUsed().setText(Integer.toString(punktyDoswiadczenia.getPunktyDoswiadczeniaRazem()));

        expieriencePanel.getAddExpButton().addActionListener(new actionListener());
    }

    private class actionListener implements ActionListener {
        actionListener() {

        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int addedExp = Integer.parseInt(expieriencePanel.getExpToAdd().getText());
            jTextArea.insert("Dodano: " + addedExp + "PD\n", 0);
            punktyDoswiadczenia.setPunktyDoswiadczeniaObecne(addedExp);
            expieriencePanel.getExpToUse().setText(Integer.toString(punktyDoswiadczenia.getPunktyDoswiadczeniaObecne()));
            expieriencePanel.getExpToAdd().setText("");
        }
    }
}
