package org.controller.main_frame.new_hero_panel_components;

import org.model.data_files.PROFESJE;
import org.model.profesje.Profesja;
import org.model.rasy.Rasa;
import org.view.main_frame.new_hero_panel_components.CareerComponent;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.Enumeration;

public class CareerComponentController {
    Class<? extends Profesja> chosenCareer;

    public CareerComponentController(CareerComponent careerComponent, JTextArea textArea) {

        // Action Listeners
        Enumeration<AbstractButton> buttonList = careerComponent.getCareerGroup().getElements();
        for (int i = 0; i<PROFESJE.PROFESJE.length; i++) {
            AbstractButton button = buttonList.nextElement();
            button.addActionListener(
                    new CareerAction(PROFESJE.PROFESJE[i], button.getText(),textArea));
        }
    }

    private class CareerAction implements ActionListener {
        private Class<? extends Profesja> career;
        private String careerName;
        private JTextArea textArea;

        CareerAction(Class<? extends Profesja> career, String careerName, JTextArea textArea) {
            this.career = career;
            this.careerName = careerName;
            this.textArea = textArea;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            chosenCareer = career;
            textArea.insert("Wybrano profesję: " + careerName + "\n",0);
        }
    }

    public Class<? extends Profesja> getChosenCareer() {
        return chosenCareer;
    }
}
