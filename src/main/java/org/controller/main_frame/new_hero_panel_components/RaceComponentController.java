package org.controller.main_frame.new_hero_panel_components;

import org.model.data_files.RASY;
import org.model.rasy.Rasa;
import org.view.main_frame.new_hero_panel_components.RaceComponent;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.Enumeration;

public class RaceComponentController {
    private Class<? extends Rasa> chosenRace;

    public RaceComponentController(RaceComponent careerComponent, JTextArea textArea) {

        // Action Listeners
        Enumeration<AbstractButton> buttonList = careerComponent.getRaceGroup().getElements();
        for (int i = 0; i< RASY.RASY.length; i++) {
            AbstractButton button = buttonList.nextElement();
            button.addActionListener(new RaceAction(RASY.RASY[i], button.getText(), textArea));
        }
    }

    private class RaceAction implements ActionListener {
        private Class<? extends Rasa> race;
        private String raceName;
        private JTextArea textArea;

        RaceAction(Class<? extends Rasa> race, String raceName, JTextArea textArea) {
            this.race = race;
            this.raceName = raceName;
            this.textArea = textArea;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            chosenRace = race;
            textArea.insert("Wybrano rasę: " + raceName + "\n",0);
        }
    }

    public Class<? extends Rasa> getChosenRace() {
        return chosenRace;
    }
}
