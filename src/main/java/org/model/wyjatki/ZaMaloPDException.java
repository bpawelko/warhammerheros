package org.model.wyjatki;

public class ZaMaloPDException extends Exception {
    public ZaMaloPDException(int obecnePD, int progPD, String miejsceUzycia) {
        super("Brakuje " + (progPD - obecnePD) + " punkow doswiadczenia do: " + miejsceUzycia);
    }
}
