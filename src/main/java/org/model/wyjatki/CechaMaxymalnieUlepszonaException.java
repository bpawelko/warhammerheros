package org.model.wyjatki;

import org.model.cechy.Cecha;

public class CechaMaxymalnieUlepszonaException extends Exception {
    public CechaMaxymalnieUlepszonaException(Cecha cecha) {
        super("Cecha: " + cecha.getNazwa() + " maksymalnie rozwinięta");
    }
}
