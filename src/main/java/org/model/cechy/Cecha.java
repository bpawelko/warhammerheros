package org.model.cechy;

public class Cecha {
    private String nazwa;
    private int wartosc;

    public Cecha(String nazwa, int wartosc) {
        this.nazwa = nazwa;
        this.wartosc = wartosc;
    }

    public String getNazwa() {
        return nazwa;
    }

    public int getWartosc() {
        return wartosc;
    }

    public void setWartosc(int wartosc) {
        this.wartosc = wartosc;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null) return false;
        if (this.getClass() != other.getClass()) return false;

        Cecha otherCecha = (Cecha) other;

        return this.nazwa.equals(otherCecha.nazwa) && this.wartosc == otherCecha.wartosc;
    }

    @Override
    public String toString() {
        return nazwa + ": " + wartosc;
    }
}
