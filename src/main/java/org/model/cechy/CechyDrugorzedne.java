package org.model.cechy;

import org.model.data_files.CECHY_DRUGORZEDNE;

public class CechyDrugorzedne {
    // TODO
    // Cechy drugorzedne jako nadklasa cech aktualnych początkowych itp
    private Cecha ataki;
    private Cecha zywotnosc;
    private Cecha sila;
    private Cecha wytrzymalosc;
    private Cecha szybkosc;
    private Cecha magia;
    private Cecha punktyObledu;
    private Cecha punktyPrzeznaczenia;

    public CechyDrugorzedne(int ataki, int zywotnosc, int sila, int wytrzymalosc,
                            int szybkosc, int magia, int punktyObledu, int punktyPrzeznaczenia) {
        this.ataki  = new Cecha(CECHY_DRUGORZEDNE.ATAKI ,ataki);
        this.zywotnosc  = new Cecha(CECHY_DRUGORZEDNE.ZYWOTNOSC ,zywotnosc);
        this.sila  = new Cecha(CECHY_DRUGORZEDNE.SILA ,sila);
        this.wytrzymalosc  = new Cecha(CECHY_DRUGORZEDNE.WYTRZYMALOSC ,wytrzymalosc);
        this.szybkosc  = new Cecha(CECHY_DRUGORZEDNE.SZYBKOSC ,szybkosc);
        this.magia  = new Cecha(CECHY_DRUGORZEDNE.MAGIA ,magia);
        this.punktyObledu = new Cecha(CECHY_DRUGORZEDNE.PUNKTY_OBLEDU , punktyObledu);
        this.punktyPrzeznaczenia = new Cecha(CECHY_DRUGORZEDNE.PUNKTY_PRZEZNACZENIA , punktyPrzeznaczenia);
    }

    public Cecha getAtaki() {
        return ataki;
    }

    public Cecha getZywotnosc() {
        return zywotnosc;
    }

    public Cecha getSila() {
        return sila;
    }

    public Cecha getWytrzymalosc() {
        return wytrzymalosc;
    }

    public Cecha getSzybkosc() {
        return szybkosc;
    }

    public Cecha getMagia() {
        return magia;
    }

    public Cecha getPunktyObledu() {
        return punktyObledu;
    }

    public Cecha getPunktyPrzeznaczenia() {
        return punktyPrzeznaczenia;
    }

    public void setAtaki(int ataki) {
        this.ataki.setWartosc(ataki);
    }

    public void setZywotnosc(int zywotnosc) {
        this.zywotnosc.setWartosc(zywotnosc);
    }

    public void setSila(int sila) {
        this.sila.setWartosc(sila);
    }

    public void setWytrzymalosc(int wytrzymalosc) {
        this.wytrzymalosc.setWartosc(wytrzymalosc);
    }

    public void setSzybkosc(int szybkosc) {
        this.szybkosc.setWartosc(szybkosc);
    }

    public void setMagia(int magia) {
        this.magia.setWartosc(magia);
    }

    public void setPunktyObledu(int punktyObledu) {
        this.punktyObledu.setWartosc(punktyObledu);
    }

    public void setPunktyPrzeznaczenia(int punktyPrzeznaczenia) {
        this.punktyPrzeznaczenia.setWartosc(punktyPrzeznaczenia);
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null) return false;
        if (this.getClass() != other.getClass()) return false;

        CechyDrugorzedne otherCechy = (CechyDrugorzedne) other;

        return this.ataki.equals(otherCechy.ataki)
                && this.zywotnosc.equals(otherCechy.zywotnosc)
                && this.sila.equals(otherCechy.sila)
                && this.wytrzymalosc.equals(otherCechy.wytrzymalosc)
                && this.szybkosc.equals(otherCechy.szybkosc)
                && this.magia.equals(otherCechy.magia)
                && this.punktyObledu.equals(otherCechy.punktyObledu)
                && this.punktyPrzeznaczenia.equals(otherCechy.punktyPrzeznaczenia);
    }

    @Override
    public String toString() {
        return ataki + " "
                + zywotnosc + " "
                + sila + " "
                + wytrzymalosc + " "
                + szybkosc + " "
                + magia + " "
                + punktyObledu + " "
                + punktyPrzeznaczenia + "\n";
    }

    public CechyDrugorzedne copy() {
        return new CechyDrugorzedne(
                ataki.getWartosc(),
                zywotnosc.getWartosc(),
                sila.getWartosc(),
                wytrzymalosc.getWartosc(),
                szybkosc.getWartosc(),
                magia.getWartosc(),
                punktyObledu.getWartosc(),
                punktyPrzeznaczenia.getWartosc()
        );
    }
}
