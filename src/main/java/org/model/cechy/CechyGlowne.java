package org.model.cechy;

import org.model.data_files.CECHY_GLOWNE;

public class CechyGlowne {
    private Cecha walkaWrecz;
    private Cecha umiejetnosciStrzeleckie;
    private Cecha krzepa;
    private Cecha odpornosc;
    private Cecha zrecznosc;
    private Cecha inteligencja;
    private Cecha silaWoli;
    private Cecha oglada;

    public CechyGlowne(int walkaWrecz, int umiejetnosciStrzeleckie, int krzepa, int odpornosc,
                       int zrecznosc, int inteligencja, int silaWoli, int oglada) {
        this.walkaWrecz = new Cecha(CECHY_GLOWNE.WALKA_WRECZ, walkaWrecz);
        this.umiejetnosciStrzeleckie = new Cecha(CECHY_GLOWNE.UMIEJETNOSCI_STRZELECKIE, umiejetnosciStrzeleckie);
        this.krzepa = new Cecha(CECHY_GLOWNE.KRZEPA, krzepa);
        this.odpornosc = new Cecha(CECHY_GLOWNE.ODPORNOSC, odpornosc);
        this.zrecznosc = new Cecha(CECHY_GLOWNE.ZRECZNOSC, zrecznosc);
        this.inteligencja = new Cecha(CECHY_GLOWNE.INTELIGENCJA, inteligencja);
        this.silaWoli = new Cecha(CECHY_GLOWNE.SILA_WOLI, silaWoli);
        this.oglada = new Cecha(CECHY_GLOWNE.OGLADA, oglada);
    }

    public Cecha getWalkaWrecz() {
        return walkaWrecz;
    }

    public Cecha getUmiejetnosciStrzeleckie() {
        return umiejetnosciStrzeleckie;
    }

    public Cecha getKrzepa() {
        return krzepa;
    }

    public Cecha getOdpornosc() {
        return odpornosc;
    }

    public Cecha getZrecznosc() {
        return zrecznosc;
    }

    public Cecha getInteligencja() {
        return inteligencja;
    }

    public Cecha getSilaWoli() {
        return silaWoli;
    }

    public Cecha getOglada() {
        return oglada;
    }

    public void setWalkaWrecz(int walkaWrecz) {
        this.walkaWrecz.setWartosc(walkaWrecz);
    }

    public void setUmiejetnosciStrzeleckie(int umiejetnosciStrzeleckie) {
        this.umiejetnosciStrzeleckie.setWartosc(umiejetnosciStrzeleckie);
    }

    public void setKrzepa(int krzepa) {
        this.krzepa.setWartosc(krzepa);
    }

    public void setOdpornosc(int odpornosc) {
        this.odpornosc.setWartosc(odpornosc);
    }

    public void setZrecznosc(int zrecznosc) {
        this.zrecznosc.setWartosc(zrecznosc);
    }

    public void setInteligencja(int inteligencja) {
        this.inteligencja.setWartosc(inteligencja);
    }

    public void setSilaWoli(int silaWoli) {
        this.silaWoli.setWartosc(silaWoli);
    }

    public void setOglada(int oglada) {
        this.oglada.setWartosc(oglada);
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null) return false;
        if (this.getClass() != other.getClass()) return false;

        CechyGlowne otherCechy = (CechyGlowne) other;
        return this.walkaWrecz.equals(otherCechy.walkaWrecz)
                && this.umiejetnosciStrzeleckie.equals(otherCechy.umiejetnosciStrzeleckie)
                && this.krzepa.equals(otherCechy.krzepa)
                && this.odpornosc.equals(otherCechy.odpornosc)
                && this.zrecznosc.equals(otherCechy.zrecznosc)
                && this.inteligencja.equals(otherCechy.inteligencja)
                && this.silaWoli.equals(otherCechy.silaWoli)
                && this.oglada.equals(otherCechy.oglada);
     }

    @Override
    public String toString() {
        return walkaWrecz + " "
                + umiejetnosciStrzeleckie + " "
                + krzepa + " "
                + odpornosc + " "
                + zrecznosc + " "
                + inteligencja + " "
                + silaWoli + " "
                + oglada + "\n";
    }

    public CechyGlowne copy() {
        return new CechyGlowne(
                walkaWrecz.getWartosc(),
                umiejetnosciStrzeleckie.getWartosc(),
                krzepa.getWartosc(),
                odpornosc.getWartosc(),
                zrecznosc.getWartosc(),
                inteligencja.getWartosc(),
                silaWoli.getWartosc(),
                oglada.getWartosc()
        );
     }
}
