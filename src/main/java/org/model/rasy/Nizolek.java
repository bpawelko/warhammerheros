package org.model.rasy;

import org.model.KostkaK10;
import org.model.cechy.CechyDrugorzedne;
import org.model.cechy.CechyGlowne;
import org.model.data_files.RASY;

public class Nizolek extends Rasa {
    public Nizolek() {
        super(
                RASY.NIZOLEK,
                new CechyGlowne(
                        10 + 2* KostkaK10.rzut(),
                        30 + 2*KostkaK10.rzut(),
                        10 + 2*KostkaK10.rzut(),
                        10 + 2*KostkaK10.rzut(),
                        30 + 2*KostkaK10.rzut(),
                        20 + 2*KostkaK10.rzut(),
                        20 + 2*KostkaK10.rzut(),
                        30 + 2*KostkaK10.rzut()),

                new CechyDrugorzedne(
                        1,
                        tabelaZywotnosci(KostkaK10.rzut(), 8, 9, 10, 11),
                        0,
                        0,
                        4,
                        0,
                        0,
                        tabelaPunktyPrzeznaczenia(KostkaK10.rzut(), 2, 2,3))
        );
    }
}
