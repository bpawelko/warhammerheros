package org.model.rasy;

import org.model.KostkaK10;
import org.model.cechy.CechyDrugorzedne;
import org.model.cechy.CechyGlowne;
import org.model.data_files.RASY;

public class Krasnolud extends Rasa {
    public Krasnolud() {
        super(
                RASY.KRASNOLUD,
                new CechyGlowne(
                        30 + 2* KostkaK10.rzut(),
                        20 + 2*KostkaK10.rzut(),
                        30 + 2*KostkaK10.rzut(),
                        20 + 2*KostkaK10.rzut(),
                        10 + 2*KostkaK10.rzut(),
                        20 + 2*KostkaK10.rzut(),
                        20 + 2*KostkaK10.rzut(),
                        10 + 2*KostkaK10.rzut()),

                new CechyDrugorzedne(
                        1,
                        tabelaZywotnosci(KostkaK10.rzut(), 11, 12, 13, 14),
                        0,
                        0,
                        3,
                        0,
                        0,
                        tabelaPunktyPrzeznaczenia(KostkaK10.rzut(), 1, 2,3))
        );
    }
}
