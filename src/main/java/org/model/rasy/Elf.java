package org.model.rasy;

import org.model.KostkaK10;
import org.model.cechy.CechyDrugorzedne;
import org.model.cechy.CechyGlowne;
import org.model.data_files.RASY;

public class Elf extends Rasa {
    public Elf() {
        super(
                RASY.ELF,
                new CechyGlowne(
                        20 + 2* KostkaK10.rzut(),
                        30 + 2*KostkaK10.rzut(),
                        20 + 2*KostkaK10.rzut(),
                        20 + 2*KostkaK10.rzut(),
                        30 + 2*KostkaK10.rzut(),
                        20 + 2*KostkaK10.rzut(),
                        20 + 2*KostkaK10.rzut(),
                        20 + 2*KostkaK10.rzut()),

                new CechyDrugorzedne(
                        1,
                        tabelaZywotnosci(KostkaK10.rzut(), 9, 10, 11, 12),
                        0,
                        0,
                        5,
                        0,
                        0,
                        tabelaPunktyPrzeznaczenia(KostkaK10.rzut(), 1, 2,2))
        );
    }
}
