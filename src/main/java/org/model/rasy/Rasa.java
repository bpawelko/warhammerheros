package org.model.rasy;

import org.model.cechy.CechyDrugorzedne;
import org.model.cechy.CechyGlowne;

public class Rasa {
    private String nazwaRasy;
    private CechyGlowne cechyGlowne;
    private CechyDrugorzedne cechyDrugorzedne;

    public Rasa(String nazwaRasy, CechyGlowne cechyGlowne, CechyDrugorzedne cechyDrugorzedne) {
        this.nazwaRasy = nazwaRasy;
        this.cechyGlowne = cechyGlowne;
        this.cechyDrugorzedne = cechyDrugorzedne;

        cechyDrugorzedne.setSila(cechyGlowne.getKrzepa().getWartosc()/10);
        cechyDrugorzedne.setWytrzymalosc(cechyGlowne.getOdpornosc().getWartosc()/10);
    }

    public static int tabelaZywotnosci(int wynikRzutu, int w1, int w2, int w3, int w4) {
        if (wynikRzutu <= 3)
            return w1;
        else if (wynikRzutu <= 6)
            return w2;
        else if (wynikRzutu <= 9)
            return w3;
        else
            return w4;
    }

    public static int tabelaPunktyPrzeznaczenia(int wynikRzutu, int w1, int w2, int w3) {
        if (wynikRzutu <= 4)
            return w1;
        else if (wynikRzutu <= 7)
            return w2;
        else
            return w3;
    }

    public String getNazwaRasy() {
        return nazwaRasy;
    }

    public CechyGlowne getCechyGlowne() {
        return cechyGlowne;
    }

    public CechyDrugorzedne getCechyDrugorzedne() {
        return cechyDrugorzedne;
    }

    @Override
    public String toString() {
        return nazwaRasy;
    }
}
