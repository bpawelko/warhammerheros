package org.model.rasy;

import org.model.KostkaK10;
import org.model.cechy.CechyDrugorzedne;
import org.model.cechy.CechyGlowne;
import org.model.data_files.RASY;

public class Czlowiek extends Rasa {
    public Czlowiek() {
        super(
              RASY.CZLOWIEK,
              new CechyGlowne(
                20 + 2* KostkaK10.rzut(),
                20 + 2*KostkaK10.rzut(),
                20 + 2*KostkaK10.rzut(),
                20 + 2*KostkaK10.rzut(),
                20 + 2*KostkaK10.rzut(),
                20 + 2*KostkaK10.rzut(),
                20 + 2*KostkaK10.rzut(),
                20 + 2*KostkaK10.rzut()),

              new CechyDrugorzedne(
                1,
                tabelaZywotnosci(KostkaK10.rzut(), 10, 11, 12, 13),
                0,
                0,
                4,
                0,
                0,
                tabelaPunktyPrzeznaczenia(KostkaK10.rzut(), 2, 3,3))
        );
    }
}
