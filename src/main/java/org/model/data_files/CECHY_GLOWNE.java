package org.model.data_files;

public class CECHY_GLOWNE {
    public static final String WALKA_WRECZ = "WalkaWrecz";
    public static final String UMIEJETNOSCI_STRZELECKIE = "UmiejetnosciStrzeleckie";
    public static final String KRZEPA = "Krzepa";
    public static final String ODPORNOSC = "Odpornosc";
    public static final String ZRECZNOSC = "Zrecznosc";
    public static final String INTELIGENCJA = "Inteligencja";
    public static final String SILA_WOLI = "SilaWoli";
    public static final String OGLADA = "Oglada";
}
