package org.model.data_files;

import org.model.rasy.Czlowiek;
import org.model.rasy.Elf;
import org.model.rasy.Krasnolud;
import org.model.rasy.Nizolek;

public class RASY {
    public static final String CZLOWIEK = "Czlowiek";
    public static final String ELF = "Elf";
    public static final String KRASNOLUD = "Krasnolud";
    public static final String NIZOLEK = "Nizolek";

    public static final Class[] RASY = new Class[] {Czlowiek.class, Elf.class, Krasnolud.class, Nizolek.class};
}
