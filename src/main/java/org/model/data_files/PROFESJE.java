package org.model.data_files;

import org.model.profesje.Akolita;
import org.model.profesje.Banita;
import org.model.profesje.Chlop;
import org.model.profesje.Profesja;

public class PROFESJE {
    public static final String AKOLITA = "Akolita";
    public static final String BANITA = "Banita";
    public static final String CHLOP = "Chłop";

    public static final Class[] PROFESJE = new Class[] {
        Akolita.class, Banita.class, Chlop.class
    };
}
