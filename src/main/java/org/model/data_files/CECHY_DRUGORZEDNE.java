package org.model.data_files;

public class CECHY_DRUGORZEDNE {
    public static final String ATAKI = "Ataki";
    public static final String ZYWOTNOSC = "Zywotnosc";
    public static final String SILA = "Sila";
    public static final String WYTRZYMALOSC = "Wytrzymalosc";
    public static final String SZYBKOSC = "Szybkosc";
    public static final String MAGIA = "Magia";
    public static final String PUNKTY_OBLEDU = "PunktyObledu";
    public static final String PUNKTY_PRZEZNACZENIA = "PunktyPrzeznaczenia";
}
