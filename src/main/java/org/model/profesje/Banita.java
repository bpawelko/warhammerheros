package org.model.profesje;

import org.model.cechy.CechyDrugorzedne;
import org.model.cechy.CechyGlowne;
import org.model.data_files.PROFESJE;

public class Banita extends Profesja {

    public Banita() {
        super(
                PROFESJE.BANITA,
                new CechyGlowne(10,10,0,0,
                        10,5,0,0),
                new CechyDrugorzedne(1,2,0,0,
                        0,0,0,0));
    }
}
