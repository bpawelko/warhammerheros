package org.model.profesje;

import org.model.cechy.CechyDrugorzedne;
import org.model.cechy.CechyGlowne;

public class Profesja {
    private String nazwaProfesji;
    private CechyGlowne cechyGlowne;
    private CechyDrugorzedne cechyDrugorzedne;

    Profesja(String nazwaProfesji, CechyGlowne cechyGlowne, CechyDrugorzedne cechyDrugorzedne) {
        this.nazwaProfesji = nazwaProfesji;
        this.cechyGlowne = cechyGlowne;
        this.cechyDrugorzedne = cechyDrugorzedne;
    }

    public CechyGlowne getCechyGlowne() {
        return cechyGlowne;
    }

    public CechyDrugorzedne getCechyDrugorzedne() {
        return cechyDrugorzedne;
    }

    @Override
    public String toString() {
        return nazwaProfesji;
    }
}
