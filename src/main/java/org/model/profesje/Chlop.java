package org.model.profesje;

import org.model.cechy.CechyDrugorzedne;
import org.model.cechy.CechyGlowne;
import org.model.data_files.PROFESJE;

public class Chlop extends Profesja {

    public Chlop() {
        super(
                PROFESJE.CHLOP,
                new CechyGlowne(5,5,5,10,
                        5,0,5,0),
                new CechyDrugorzedne(0,2,0,0,
                        0,0,0,0));
    }
}
