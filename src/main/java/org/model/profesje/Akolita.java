package org.model.profesje;

import org.model.cechy.CechyDrugorzedne;
import org.model.cechy.CechyGlowne;
import org.model.data_files.PROFESJE;

public class Akolita extends Profesja {

    public Akolita() {
        super(
                PROFESJE.AKOLITA,
                new CechyGlowne(5,5,0,5,
                        0,10,10,10),
                new CechyDrugorzedne(0,2,0,0,
                        0,0,0,0));
    }
}
