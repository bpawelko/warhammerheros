package org.model.postacie.postac_gracza;

import org.model.data_files.POSTACIE;
import org.model.postacie.Postac;
import org.model.postacie.postac_gracza.statystyki.CechySpecjalne;
import org.model.postacie.postac_gracza.statystyki.PodstawoweInformacje;
import org.model.postacie.postac_gracza.statystyki.PunktyDoswiadczenia;
import org.model.profesje.Profesja;
import org.model.rasy.Rasa;

public class PostacGracza extends Postac {
    private PodstawoweInformacje podstawoweInformacje;
    private CechySpecjalne cechySpecjalne;
    private PunktyDoswiadczenia punktyDoswiadczenia;

    public PostacGracza(String imie, Rasa rasa, Profesja profesja) {
        super(POSTACIE.POSTAC_GRACZA);
        podstawoweInformacje = new PodstawoweInformacje(
                imie,rasa,profesja,null);
        cechySpecjalne = new CechySpecjalne(
                rasa.getCechyGlowne(),
                profesja.getCechyGlowne(),
                rasa.getCechyDrugorzedne(),
                profesja.getCechyDrugorzedne()
        );
        punktyDoswiadczenia = new PunktyDoswiadczenia();
    }

    public PodstawoweInformacje getPodstawoweInformacje() {
        return podstawoweInformacje;
    }

    public CechySpecjalne getCechySpecjalne() {
        return cechySpecjalne;
    }

    public PunktyDoswiadczenia getPunktyDoswiadczenia() {
        return punktyDoswiadczenia;
    }

    @Override
    public String toString() {
        return "" + podstawoweInformacje
                + punktyDoswiadczenia
                + cechySpecjalne;
    }
}
