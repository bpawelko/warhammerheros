package org.model.postacie.postac_gracza.narzedzia_funkcje;

import org.model.cechy.Cecha;
import org.model.cechy.CechyDrugorzedne;
import org.model.cechy.CechyGlowne;
import org.model.data_files.CECHY_GLOWNE;
import org.model.wyjatki.CechaMaxymalnieUlepszonaException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class UlepszenieCechyTools {

    public static void zmienWartocCechy(Cecha cecha, int wartosc) {
        // Zmienia wartosc cechy o liczbe
        int aktualnaWartosc = cecha.getWartosc();
        cecha.setWartosc(aktualnaWartosc + wartosc);
    }

    public static void zmienCechyAktualneISchematu(Cecha cechaAktualna, Cecha cechaSchematuRoznicowego, int wartosc) {
        // Dodaje wartosc do cechy aktualnej, odejmuje wartosc ze schematu roznicowego
        zmienWartocCechy(cechaAktualna,wartosc);
        zmienWartocCechy(cechaSchematuRoznicowego, -wartosc);
    }

    public static void sprawdzMozliwoscUlepszenia(Cecha cechaSchematuRoznicowego)
            throws CechaMaxymalnieUlepszonaException
    {
        if (cechaSchematuRoznicowego.getWartosc() < 1)
            throw new CechaMaxymalnieUlepszonaException(cechaSchematuRoznicowego);
    }

    public static Cecha getCechaGlownaFromString(String nazwaCechy, CechyGlowne cechyGlowne)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException
    {
        Method getCecha = CechyGlowne.class.getMethod("get" + nazwaCechy);
        return (Cecha) getCecha.invoke(cechyGlowne);
    }

    public static Cecha getCechaDrugorzednaFromString(String nazwaCechy, CechyDrugorzedne cechyDrugorzedne)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException
    {
        Method getCecha = CechyDrugorzedne.class.getMethod("get" + nazwaCechy);
        return (Cecha) getCecha.invoke(cechyDrugorzedne);
    }

    public static void aktualizujCechyDrugorzedne(Cecha cechaAktualna, CechyDrugorzedne cechyAktualne) {
        if (cechaAktualna.getNazwa().equals(CECHY_GLOWNE.KRZEPA))
            cechyAktualne.setSila(cechaAktualna.getWartosc()/10);

        if (cechaAktualna.getNazwa().equals(CECHY_GLOWNE.ODPORNOSC))
            cechyAktualne.setWytrzymalosc(cechaAktualna.getWartosc()/10);
    }
}