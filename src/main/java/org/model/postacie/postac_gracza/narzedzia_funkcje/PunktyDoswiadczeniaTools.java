package org.model.postacie.postac_gracza.narzedzia_funkcje;

import org.model.postacie.postac_gracza.statystyki.PunktyDoswiadczenia;
import org.model.wyjatki.ZaMaloPDException;

public class PunktyDoswiadczeniaTools {
    public static void sprawdzPD(PunktyDoswiadczenia punktyDoswiadczenia, int progPD, String miejsceUzycia)
            throws ZaMaloPDException
    {
        if (punktyDoswiadczenia.getPunktyDoswiadczeniaObecne() < progPD)
            throw new ZaMaloPDException(punktyDoswiadczenia.getPunktyDoswiadczeniaObecne(), progPD, miejsceUzycia);
    }

    public static void wykorzystajPD(PunktyDoswiadczenia punktyDoswiadczenia, int progPD) {
        int punktyObecne = punktyDoswiadczenia.getPunktyDoswiadczeniaObecne();
        punktyDoswiadczenia.setPunktyDoswiadczeniaObecne(punktyObecne - progPD);

        int punktyRazem = punktyDoswiadczenia.getPunktyDoswiadczeniaRazem();
        punktyDoswiadczenia.setPunktyDoswiadczeniaRazem(punktyRazem + progPD);
    }
}