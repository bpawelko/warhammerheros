package org.model.postacie.postac_gracza.statystyki;

import org.model.cechy.CechyDrugorzedne;
import org.model.cechy.CechyGlowne;

public class CechySpecjalne {
    private CechyGlowne cechyGlownePoczatkowe;
    private CechyGlowne cechyGlowneSchematRozwoju;
    private CechyGlowne cechyGlowneSchematRoznicowy;
    private CechyGlowne cechyGlowneAktualne;

    private CechyDrugorzedne cechyDrugorzednePoczatkowe;
    private CechyDrugorzedne cechyDrugorzedneSchematRozwoju;
    private CechyDrugorzedne cechyDrugorzedneSchematRoznicowy;
    private CechyDrugorzedne cechyDrugorzedneAktualne;

    public CechySpecjalne(CechyGlowne cechyGlownePoczatkowe, CechyGlowne cechyGlowneSchematRozwoju,
                          CechyDrugorzedne cechyDrugorzednePoczatkowe, CechyDrugorzedne cechyDrugorzedneSchematRozwoju) {

        this.cechyGlownePoczatkowe = cechyGlownePoczatkowe;
        this.cechyGlowneSchematRozwoju = cechyGlowneSchematRozwoju;
        this.cechyGlowneSchematRoznicowy = cechyGlowneSchematRozwoju.copy();
        this.cechyGlowneAktualne = cechyGlownePoczatkowe.copy();

        this.cechyDrugorzednePoczatkowe = cechyDrugorzednePoczatkowe;
        this.cechyDrugorzedneSchematRozwoju = cechyDrugorzedneSchematRozwoju;
        this.cechyDrugorzedneSchematRoznicowy = cechyDrugorzedneSchematRozwoju.copy();
        this.cechyDrugorzedneAktualne = cechyDrugorzednePoczatkowe.copy();
    }

    public CechyGlowne getCechyGlownePoczatkowe() {
        return cechyGlownePoczatkowe;
    }

    public CechyGlowne getCechyGlowneSchematRozwoju() {
        return cechyGlowneSchematRozwoju;
    }

    public CechyGlowne getCechyGlowneSchematRoznicowy() {
        return cechyGlowneSchematRoznicowy;
    }

    public CechyGlowne getCechyGlowneAktualne() {
        return cechyGlowneAktualne;
    }

    public CechyDrugorzedne getCechyDrugorzednePoczatkowe() {
        return cechyDrugorzednePoczatkowe;
    }

    public CechyDrugorzedne getCechyDrugorzedneSchematRozwoju() {
        return cechyDrugorzedneSchematRozwoju;
    }

    public CechyDrugorzedne getCechyDrugorzedneSchematRoznicowy() {
        return cechyDrugorzedneSchematRoznicowy;
    }

    public CechyDrugorzedne getCechyDrugorzedneAktualne() {
        return cechyDrugorzedneAktualne;
    }

    public void setCechyGlownePoczatkowe(CechyGlowne cechyGlownePoczatkowe) {
        this.cechyGlownePoczatkowe = cechyGlownePoczatkowe;
    }

    public void setCechyGlowneSchematRozwoju(CechyGlowne cechyGlowneSchematRozwoju) {
        this.cechyGlowneSchematRozwoju = cechyGlowneSchematRozwoju;
    }

    public void setCechyGlowneSchematRoznicowy(CechyGlowne cechyGlowneSchematRoznicowy) {
        this.cechyGlowneSchematRoznicowy = cechyGlowneSchematRoznicowy;
    }

    public void setCechyGlowneAktualne(CechyGlowne cechyGlowneAktualne) {
        this.cechyGlowneAktualne = cechyGlowneAktualne;
    }

    public void setCechyDrugorzednePoczatkowe(CechyDrugorzedne cechyDrugorzednePoczatkowe) {
        this.cechyDrugorzednePoczatkowe = cechyDrugorzednePoczatkowe;
    }

    public void setCechyDrugorzedneSchematRozwoju(CechyDrugorzedne cechyDrugorzedneSchematRozwoju) {
        this.cechyDrugorzedneSchematRozwoju = cechyDrugorzedneSchematRozwoju;
    }

    public void setCechyDrugorzedneSchematRoznicowy(CechyDrugorzedne cechyDrugorzedneSchematRoznicowy) {
        this.cechyDrugorzedneSchematRoznicowy = cechyDrugorzedneSchematRoznicowy;
    }

    public void setCechyDrugorzedneAktualne(CechyDrugorzedne cechyDrugorzedneAktualne) {
        this.cechyDrugorzedneAktualne = cechyDrugorzedneAktualne;
    }

    @Override
    public String toString() {
        return "Cechy Glowne: " + "\n"
                + cechyGlownePoczatkowe
                + cechyGlowneSchematRoznicowy
                + cechyGlowneAktualne
                + "Cechy Drugorzedne: " + "\n"
                + cechyDrugorzednePoczatkowe
                + cechyDrugorzedneSchematRoznicowy
                + cechyDrugorzedneAktualne;
    }
}
