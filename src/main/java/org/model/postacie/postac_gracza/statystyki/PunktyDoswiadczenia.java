package org.model.postacie.postac_gracza.statystyki;

public class PunktyDoswiadczenia {
    int punktyDoswiadczeniaObecne;
    int punktyDoswiadczeniaRazem;

    public PunktyDoswiadczenia() {
        this.punktyDoswiadczeniaObecne = 0;
        this.punktyDoswiadczeniaRazem = 0;
    }

    PunktyDoswiadczenia(int punktyDoswiadczeniaObecne, int punktyDoswiadczeniaRazem) {
        this.punktyDoswiadczeniaObecne = punktyDoswiadczeniaObecne;
        this.punktyDoswiadczeniaRazem = punktyDoswiadczeniaRazem;
    }

    public int getPunktyDoswiadczeniaObecne() {
        return punktyDoswiadczeniaObecne;
    }

    public int getPunktyDoswiadczeniaRazem() {
        return punktyDoswiadczeniaRazem;
    }

    public void setPunktyDoswiadczeniaObecne(int punktyDoswiadczeniaObecne) {
        this.punktyDoswiadczeniaObecne = punktyDoswiadczeniaObecne;
    }

    public void setPunktyDoswiadczeniaRazem(int punktyDoswiadczeniaRazem) {
        this.punktyDoswiadczeniaRazem = punktyDoswiadczeniaRazem;
    }

    @Override
    public String toString() {
        return "PunktyDoswiadczenia" + "\n"
                + "Obecne: " + punktyDoswiadczeniaObecne + " "
                + "Razem: " + punktyDoswiadczeniaRazem + "\n";
    }
}
