package org.model.postacie.postac_gracza.statystyki;

import org.model.profesje.Profesja;
import org.model.rasy.Rasa;

public class PodstawoweInformacje {
    private String imie;
    private Rasa rasa;
    private Profesja obecnaProfesja;
    private Profesja poprzedniaProfesja;

    public PodstawoweInformacje(String imie, Rasa rasa, Profesja obecnaProfesja, Profesja poprzedniaProfesja) {
        this.imie = imie;
        this.rasa = rasa;
        this.obecnaProfesja = obecnaProfesja;
        this.poprzedniaProfesja = poprzedniaProfesja;
    }

    public String getImie() {
        return imie;
    }

    public Rasa getRasa() {
        return rasa;
    }

    public Profesja getObecnaProfesja() {
        return obecnaProfesja;
    }

    public Profesja getPoprzedniaProfesja() {
        return poprzedniaProfesja;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setObecnaProfesja(Profesja obecnaProfesja) {
        this.obecnaProfesja = obecnaProfesja;
    }

    public void setPoprzedniaProfesja(Profesja poprzedniaProfesja) {
        this.poprzedniaProfesja = poprzedniaProfesja;
    }

    @Override
    public String toString() {
        return "Bohater" + "\n"
                + "Imie: " + imie +"\n"
                + "Rasa: " + rasa + "\n"
                + "Obecna Profesja: " + obecnaProfesja + "\n"
                + "Poprzednia Profesja: " + poprzedniaProfesja + "\n";
    }
}
