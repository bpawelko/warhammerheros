package org.model.postacie.postac_gracza;

import org.model.cechy.Cecha;
import org.model.data_files.CECHY_GLOWNE;
import org.model.data_files.METODY;
import org.model.postacie.postac_gracza.narzedzia_funkcje.PunktyDoswiadczeniaTools;
import org.model.postacie.postac_gracza.narzedzia_funkcje.UlepszenieCechyTools;
import org.model.wyjatki.CechaMaxymalnieUlepszonaException;
import org.model.wyjatki.ZaMaloPDException;

import java.lang.reflect.InvocationTargetException;

public class UlepszenieCech {

    public static void UlepszCecheGlowna(PostacGracza postacGracza, String nazwaCechy)
            throws ZaMaloPDException, CechaMaxymalnieUlepszonaException {
        try {
            // Sprawdzenie czy posiadana jest wystarczająca ilość PD
            PunktyDoswiadczeniaTools.sprawdzPD(
                    postacGracza.getPunktyDoswiadczenia(),
                    100,
                    METODY.ULEPSZENIE_CECHY_GLOWNEJ);

            // Pobranie cech za pomocą generycznych metod i sprawdzenie czy wybrana cecha może zostać ulepszona
            Cecha cechaSchematRoznicowy = UlepszenieCechyTools.getCechaGlownaFromString(
                    nazwaCechy,
                    postacGracza.getCechySpecjalne().getCechyGlowneSchematRoznicowy());

            Cecha cechaAktualna = UlepszenieCechyTools.getCechaGlownaFromString(
                    nazwaCechy,
                    postacGracza.getCechySpecjalne().getCechyGlowneAktualne());

            UlepszenieCechyTools.sprawdzMozliwoscUlepszenia(cechaSchematRoznicowy);

            // Ulepszenie cech
            UlepszenieCechyTools.zmienCechyAktualneISchematu(
                    cechaAktualna,cechaSchematRoznicowy,5
            );

            // Zaktualizowanie cech drugorzędnych jeżeli ulepszana cecha głowna wpływa na wartość cechy drugorzędnej
            if (nazwaCechy.equals(CECHY_GLOWNE.KRZEPA) || nazwaCechy.equals(CECHY_GLOWNE.ODPORNOSC))
                UlepszenieCechyTools.aktualizujCechyDrugorzedne(
                        cechaAktualna,
                        postacGracza.getCechySpecjalne().getCechyDrugorzedneAktualne()
                );

            // Wydanie PD na ulepszenie
            PunktyDoswiadczeniaTools.wykorzystajPD(
                    postacGracza.getPunktyDoswiadczenia(), 100);

        }  catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static void UlepszCecheDrugorzedna(PostacGracza postacGracza, String nazwaCechy)
    throws CechaMaxymalnieUlepszonaException, ZaMaloPDException {
        try {
            // Sprawdzenie czy posiadana jest wystarczająca ilość PD
            PunktyDoswiadczeniaTools.sprawdzPD(
                    postacGracza.getPunktyDoswiadczenia(),
                    100,
                    METODY.ULEPSZENIE_CECHY_DRUGORZEDNEJ);

            // Pobranie cech za pomocą generycznych metod i sprawdzenie czy wybrana cecha może zostać ulepszona
            Cecha cechaSchematRoznicowy = UlepszenieCechyTools.getCechaDrugorzednaFromString(
                    nazwaCechy,
                    postacGracza.getCechySpecjalne().getCechyDrugorzedneSchematRoznicowy());

            Cecha cechaAktualna = UlepszenieCechyTools.getCechaDrugorzednaFromString(
                    nazwaCechy,
                    postacGracza.getCechySpecjalne().getCechyDrugorzedneAktualne());

            UlepszenieCechyTools.sprawdzMozliwoscUlepszenia(cechaSchematRoznicowy);

            // Ulepszenie cech
            UlepszenieCechyTools.zmienCechyAktualneISchematu(
                    cechaAktualna,cechaSchematRoznicowy,1
            );

            // Wydanie PD na ulepszenie
            PunktyDoswiadczeniaTools.wykorzystajPD(
                    postacGracza.getPunktyDoswiadczenia(), 100);

        }  catch (NoSuchMethodException
                  | InvocationTargetException
                  | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
