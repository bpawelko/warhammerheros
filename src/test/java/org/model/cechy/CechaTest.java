package org.model.cechy;

import org.junit.Assert;
import org.junit.Test;

public class CechaTest {

    @Test
    public void getNazwa() {
        Cecha cecha = new Cecha("test", 0);
        Assert.assertEquals("test", cecha.getNazwa());
    }

    @Test
    public void getWartosc() {
        Cecha cecha = new Cecha("test", 0);
        Assert.assertEquals(0,cecha.getWartosc());
    }

    @Test
    public void eqaulsSameObject() {
        Cecha cecha = new Cecha("test",0);
        Assert.assertEquals(cecha, cecha);
    }

    @Test
    public void eqaulsNull() {
        Cecha cecha = new Cecha("test",0);
        Assert.assertNotEquals(null, cecha);
    }

    @Test
    public void eqaulsObjectDifferentClass() {
        Cecha cecha = new Cecha("test",0);
        Object object = new Object();
        Assert.assertNotEquals(cecha, object);
    }

    @Test
    public void eqaulsObjectTrue() {
        Cecha cecha = new Cecha("test",0);
        Cecha cecha2 = new Cecha("test",0);
        Assert.assertEquals(cecha, cecha2);
    }

    @Test
    public void eqaulsObjectFalse() {
        Cecha cecha = new Cecha("test",0);
        Cecha cecha2 = new Cecha("test2",0);
        Cecha cecha3 = new Cecha("test",1);
        Cecha cecha4 = new Cecha("test2",1);
        Assert.assertNotEquals(cecha, cecha2);
        Assert.assertNotEquals(cecha, cecha3);
        Assert.assertNotEquals(cecha, cecha4);
    }
}
