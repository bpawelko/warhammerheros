package org.model.cechy;

import org.model.data_files.CECHY_GLOWNE;
import org.junit.Assert;
import org.junit.Test;

public class CechyGlowneTest {

    @Test
    public void getCecha() {
        CechyGlowne cechy_glowne = new CechyGlowne(0,0,0,0,
                0,0,0,0);
        Assert.assertEquals(new Cecha(CECHY_GLOWNE.WALKA_WRECZ,0), cechy_glowne.getWalkaWrecz());
        Assert.assertEquals(new Cecha(CECHY_GLOWNE.UMIEJETNOSCI_STRZELECKIE,0), cechy_glowne.getUmiejetnosciStrzeleckie());
        Assert.assertEquals(new Cecha(CECHY_GLOWNE.KRZEPA,0), cechy_glowne.getKrzepa());
        Assert.assertEquals(new Cecha(CECHY_GLOWNE.ODPORNOSC,0), cechy_glowne.getOdpornosc());
        Assert.assertEquals(new Cecha(CECHY_GLOWNE.ZRECZNOSC,0), cechy_glowne.getZrecznosc());
        Assert.assertEquals(new Cecha(CECHY_GLOWNE.INTELIGENCJA,0), cechy_glowne.getInteligencja());
        Assert.assertEquals(new Cecha(CECHY_GLOWNE.SILA_WOLI,0), cechy_glowne.getSilaWoli());
        Assert.assertEquals(new Cecha(CECHY_GLOWNE.OGLADA,0), cechy_glowne.getOglada());
    }

    @Test
    public void setCecha() {
        CechyGlowne cechy_glowne = new CechyGlowne(0,0,0,0,
                0,0,0,0);

        cechy_glowne.setWalkaWrecz(1);
        cechy_glowne.setUmiejetnosciStrzeleckie(1);
        cechy_glowne.setKrzepa(1);
        cechy_glowne.setOdpornosc(1);
        cechy_glowne.setZrecznosc(1);
        cechy_glowne.setInteligencja(1);
        cechy_glowne.setSilaWoli(1);
        cechy_glowne.setOglada(1);

        Assert.assertEquals(1, cechy_glowne.getWalkaWrecz().getWartosc());
        Assert.assertEquals(1, cechy_glowne.getUmiejetnosciStrzeleckie().getWartosc());
        Assert.assertEquals(1, cechy_glowne.getKrzepa().getWartosc());
        Assert.assertEquals(1, cechy_glowne.getOdpornosc().getWartosc());
        Assert.assertEquals(1, cechy_glowne.getZrecznosc().getWartosc());
        Assert.assertEquals(1, cechy_glowne.getInteligencja().getWartosc());
        Assert.assertEquals(1, cechy_glowne.getSilaWoli().getWartosc());
        Assert.assertEquals(1, cechy_glowne.getOglada().getWartosc());
    }

    @Test
    public void equalsSameObject() {
        CechyGlowne cechy_glowne = new CechyGlowne(0,0,0,0,
                0,0,0,0);
        Assert.assertEquals(cechy_glowne, cechy_glowne);
    }

    @Test
    public void equalsNull() {
        CechyGlowne cechy_glowne = new CechyGlowne(0,0,0,0,
                0,0,0,0);
        Assert.assertNotEquals(cechy_glowne, null);
    }

    @Test
    public void eqaulsObjectDifferentClass() {
        CechyGlowne cechy_glowne = new CechyGlowne(0,0,0,0,
                0,0,0,0);
        Object object = new Object();
        Assert.assertNotEquals(cechy_glowne, object);
    }

    @Test
    public void eqaulsObjectTrue() {
        CechyGlowne cechy_glowne = new CechyGlowne(0,0,0,0,
                0,0,0,0);
        CechyGlowne cechy_glowne2 = new CechyGlowne(0,0,0,0,
                0,0,0,0);
        Assert.assertEquals(cechy_glowne, cechy_glowne2);
    }

    @Test
    public void eqaulsObjectFalse() {
        CechyGlowne cechy_glowne = new CechyGlowne(0,0,0,0,
                0,0,0,0);
        CechyGlowne cechy_glowne2 = new CechyGlowne(1,0,0,0,
                0,0,0,0);
        CechyGlowne cechy_glowne3 = new CechyGlowne(1,1,0,0,
                0,0,0,0);
        CechyGlowne cechy_glowne4 = new CechyGlowne(1,0,1,0,
                0,0,0,0);
        CechyGlowne cechy_glowne5 = new CechyGlowne(1,0,0,1,
                0,0,0,0);
        CechyGlowne cechy_glowne6 = new CechyGlowne(1,0,0,0,
                1,0,0,0);
        CechyGlowne cechy_glowne7 = new CechyGlowne(1,0,0,0,
                0,1,0,0);
        CechyGlowne cechy_glowne8 = new CechyGlowne(1,0,0,0,
                0,0,1,0);
        CechyGlowne cechy_glowne9 = new CechyGlowne(1,0,0,0,
                0,0,0,1);

        Assert.assertNotEquals(cechy_glowne, cechy_glowne2);
        Assert.assertNotEquals(cechy_glowne, cechy_glowne3);
        Assert.assertNotEquals(cechy_glowne, cechy_glowne4);
        Assert.assertNotEquals(cechy_glowne, cechy_glowne5);
        Assert.assertNotEquals(cechy_glowne, cechy_glowne6);
        Assert.assertNotEquals(cechy_glowne, cechy_glowne7);
        Assert.assertNotEquals(cechy_glowne, cechy_glowne8);
        Assert.assertNotEquals(cechy_glowne, cechy_glowne9);
    }

    @Test
    public void copy() {
        CechyGlowne cechy_glowne = new CechyGlowne(0,0,0,0,
                0,0,0,0);
        CechyGlowne cechy_glowne2 = cechy_glowne.copy();
        Assert.assertEquals(cechy_glowne, cechy_glowne2);
    }
}
