package org.model.cechy;

import org.model.data_files.CECHY_DRUGORZEDNE;
import org.junit.Assert;
import org.junit.Test;

public class CechyDrugorzedneTest {

    @Test
    public void getCecha() {
        CechyDrugorzedne cechy_drugorzedne = new CechyDrugorzedne(0,0,0,0,
                0,0,0,0);
        Assert.assertEquals(new Cecha(CECHY_DRUGORZEDNE.ATAKI,0), cechy_drugorzedne.getAtaki());
        Assert.assertEquals(new Cecha(CECHY_DRUGORZEDNE.ZYWOTNOSC,0), cechy_drugorzedne.getZywotnosc());
        Assert.assertEquals(new Cecha(CECHY_DRUGORZEDNE.SILA,0), cechy_drugorzedne.getSila());
        Assert.assertEquals(new Cecha(CECHY_DRUGORZEDNE.WYTRZYMALOSC,0), cechy_drugorzedne.getWytrzymalosc());
        Assert.assertEquals(new Cecha(CECHY_DRUGORZEDNE.SZYBKOSC,0), cechy_drugorzedne.getSzybkosc());
        Assert.assertEquals(new Cecha(CECHY_DRUGORZEDNE.MAGIA,0), cechy_drugorzedne.getMagia());
        Assert.assertEquals(new Cecha(CECHY_DRUGORZEDNE.PUNKTY_OBLEDU,0), cechy_drugorzedne.getPunktyObledu());
        Assert.assertEquals(new Cecha(CECHY_DRUGORZEDNE.PUNKTY_PRZEZNACZENIA,0), cechy_drugorzedne.getPunktyPrzeznaczenia());
    }

    @Test
    public void setCecha() {
        CechyDrugorzedne cechy_drugorzedne = new CechyDrugorzedne(0,0,0,0,
                0,0,0,0);

        cechy_drugorzedne.setAtaki(1);
        cechy_drugorzedne.setZywotnosc(1);
        cechy_drugorzedne.setSila(1);
        cechy_drugorzedne.setWytrzymalosc(1);
        cechy_drugorzedne.setSzybkosc(1);
        cechy_drugorzedne.setMagia(1);
        cechy_drugorzedne.setPunktyObledu(1);
        cechy_drugorzedne.setPunktyPrzeznaczenia(1);

        Assert.assertEquals(1, cechy_drugorzedne.getAtaki().getWartosc());
        Assert.assertEquals(1, cechy_drugorzedne.getZywotnosc().getWartosc());
        Assert.assertEquals(1, cechy_drugorzedne.getSila().getWartosc());
        Assert.assertEquals(1, cechy_drugorzedne.getWytrzymalosc().getWartosc());
        Assert.assertEquals(1, cechy_drugorzedne.getSzybkosc().getWartosc());
        Assert.assertEquals(1, cechy_drugorzedne.getMagia().getWartosc());
        Assert.assertEquals(1, cechy_drugorzedne.getPunktyObledu().getWartosc());
        Assert.assertEquals(1, cechy_drugorzedne.getPunktyPrzeznaczenia().getWartosc());
    }

    @Test
    public void equalsSameObject() {
        CechyDrugorzedne cechy_drugorzedne = new CechyDrugorzedne(0,0,0,0,
                0,0,0,0);
        Assert.assertEquals(cechy_drugorzedne, cechy_drugorzedne);
    }

    @Test
    public void equalsNull() {
        CechyDrugorzedne cechy_drugorzedne = new CechyDrugorzedne(0,0,0,0,
                0,0,0,0);
        Assert.assertNotEquals(cechy_drugorzedne, null);
    }

    @Test
    public void eqaulsObjectDifferentClass() {
        CechyDrugorzedne cechy_drugorzedne = new CechyDrugorzedne(0,0,0,0,
                0,0,0,0);
        Object object = new Object();
        Assert.assertNotEquals(cechy_drugorzedne, object);
    }

    @Test
    public void eqaulsObjectTrue() {
        CechyDrugorzedne cechy_drugorzedne = new CechyDrugorzedne(0,0,0,0,
                0,0,0,0);
        CechyDrugorzedne cechy_drugorzedne2 = new CechyDrugorzedne(0,0,0,0,
                0,0,0,0);
        Assert.assertEquals(cechy_drugorzedne, cechy_drugorzedne2);
    }

    @Test
    public void eqaulsObjectFalse() {
        CechyDrugorzedne cechy_drugorzedne = new CechyDrugorzedne(0,0,0,0,
                0,0,0,0);
        CechyDrugorzedne cechy_drugorzedne2 = new CechyDrugorzedne(1,0,0,0,
                0,0,0,0);
        CechyDrugorzedne cechy_drugorzedne3 = new CechyDrugorzedne(0,1,0,0,
                0,0,0,0);
        CechyDrugorzedne cechy_drugorzedne4 = new CechyDrugorzedne(0,0,1,0,
                0,0,0,0);
        CechyDrugorzedne cechy_drugorzedne5 = new CechyDrugorzedne(0,0,0,1,
                0,0,0,0);
        CechyDrugorzedne cechy_drugorzedne6 = new CechyDrugorzedne(0,0,0,0,
                1,0,0,0);
        CechyDrugorzedne cechy_drugorzedne7 = new CechyDrugorzedne(0,0,0,0,
                0,1,0,0);
        CechyDrugorzedne cechy_drugorzedne8 = new CechyDrugorzedne(0,0,0,0,
                0,0,1,0);
        CechyDrugorzedne cechy_drugorzedne9 = new CechyDrugorzedne(0,0,0,0,
                0,0,0,1);

        Assert.assertNotEquals(cechy_drugorzedne, cechy_drugorzedne2);
        Assert.assertNotEquals(cechy_drugorzedne, cechy_drugorzedne3);
        Assert.assertNotEquals(cechy_drugorzedne, cechy_drugorzedne4);
        Assert.assertNotEquals(cechy_drugorzedne, cechy_drugorzedne5);
        Assert.assertNotEquals(cechy_drugorzedne, cechy_drugorzedne6);
        Assert.assertNotEquals(cechy_drugorzedne, cechy_drugorzedne7);
        Assert.assertNotEquals(cechy_drugorzedne, cechy_drugorzedne8);
        Assert.assertNotEquals(cechy_drugorzedne, cechy_drugorzedne9);
    }

    @Test
    public void copy() {
        CechyDrugorzedne cechy_drugorzedne = new CechyDrugorzedne(0,0,0,0,
                0,0,0,0);
        CechyDrugorzedne cechy_drugorzedne2 = cechy_drugorzedne.copy();
        Assert.assertEquals(cechy_drugorzedne, cechy_drugorzedne2);
    }
}
