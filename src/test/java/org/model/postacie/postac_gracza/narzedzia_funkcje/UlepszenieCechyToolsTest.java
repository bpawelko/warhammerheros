package org.model.postacie.postac_gracza.narzedzia_funkcje;

import org.model.cechy.Cecha;
import org.model.cechy.CechyDrugorzedne;
import org.model.cechy.CechyGlowne;
import org.model.wyjatki.CechaMaxymalnieUlepszonaException;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;

public class UlepszenieCechyToolsTest {

    @Test
    public void zmienWartoscCechy() {
        Cecha cecha = new Cecha("Test", 10);
        UlepszenieCechyTools.zmienWartocCechy(cecha,5);
        Assert.assertEquals(15,cecha.getWartosc());

        UlepszenieCechyTools.zmienWartocCechy(cecha,-5);
        Assert.assertEquals(10,cecha.getWartosc());
    }

    @Test
    public void zmienCechyAktualneISchematu() {
        Cecha cechaAktualna = new Cecha("Test",10);
        Cecha cechaSchematuRoznicowego = new Cecha("Test",10);

        UlepszenieCechyTools.zmienCechyAktualneISchematu(
                cechaAktualna, cechaSchematuRoznicowego, 5);

        Assert.assertEquals(15,cechaAktualna.getWartosc());
        Assert.assertEquals(5,cechaSchematuRoznicowego.getWartosc());
    }

    @Test
    public void sprawdzMozliwoscUlepszenia() {
        Cecha cechaSchematuRoznicowego = new Cecha("Test", 1);
        try { UlepszenieCechyTools.sprawdzMozliwoscUlepszenia(cechaSchematuRoznicowego); }
        catch(CechaMaxymalnieUlepszonaException e) {
            e.printStackTrace();
        }
    }

    @Test(expected = CechaMaxymalnieUlepszonaException.class)
    public void sprawdzMozliwoscUlepszeniaException() throws CechaMaxymalnieUlepszonaException {
        Cecha cechaSchematuRoznicowego = new Cecha("Test", 0);
            UlepszenieCechyTools.sprawdzMozliwoscUlepszenia(cechaSchematuRoznicowego);
    }

    @Test
    public void getCechaGlownaFromString() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        String nazwaCechy1 = "Krzepa";
        CechyGlowne cechyGlowne = new CechyGlowne(
                1, 2, 3, 4, 5, 6, 7, 8);
        Cecha cecha1 = UlepszenieCechyTools.getCechaGlownaFromString(nazwaCechy1, cechyGlowne);
        Assert.assertEquals(cechyGlowne.getKrzepa(), cecha1);
    }

    @Test
    public void getCechaDrugorzednaFromString() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        String nazwaCechy2 = "Magia";
        CechyDrugorzedne cechyDrugorzedne = new CechyDrugorzedne(
                1,2,3,4,5,6,7,8);
        Cecha cecha2 = UlepszenieCechyTools.getCechaDrugorzednaFromString(nazwaCechy2,cechyDrugorzedne);
        Assert.assertEquals(cechyDrugorzedne.getMagia(),cecha2);
    }

    @Test(expected = NoSuchMethodException.class)
    public void getCechaGlownaFromStringException() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        String nazwaCechy1 = "Test";
        CechyGlowne cechyGlowne = new CechyGlowne(
                1, 2, 3, 4, 5, 6, 7, 8);
        Cecha cecha1 = UlepszenieCechyTools.getCechaGlownaFromString(nazwaCechy1, cechyGlowne);
    }

    @Test(expected = NoSuchMethodException.class)
    public void getCechaDrugorzednaFromStringException() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        String nazwaCechy2 = "Test";
        CechyDrugorzedne cechyDrugorzedne = new CechyDrugorzedne(
                1,2,3,4,5,6,7,8);
        Cecha cecha2 = UlepszenieCechyTools.getCechaDrugorzednaFromString(nazwaCechy2,cechyDrugorzedne);
    }

}
