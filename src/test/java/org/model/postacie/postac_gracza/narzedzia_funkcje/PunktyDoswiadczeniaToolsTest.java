package org.model.postacie.postac_gracza.narzedzia_funkcje;

import org.model.postacie.postac_gracza.statystyki.PunktyDoswiadczenia;
import org.model.wyjatki.ZaMaloPDException;
import org.junit.Assert;
import org.junit.Test;

public class PunktyDoswiadczeniaToolsTest {

    @Test
    public void sprawdzPD() throws ZaMaloPDException {
        PunktyDoswiadczenia punktyDoswiadczenia = new PunktyDoswiadczenia();
        punktyDoswiadczenia.setPunktyDoswiadczeniaObecne(100);
        PunktyDoswiadczeniaTools.sprawdzPD(punktyDoswiadczenia, 100, "Test");
    }

    @Test(expected = ZaMaloPDException.class)
    public void sprawdzPDException() throws ZaMaloPDException {
        PunktyDoswiadczenia punktyDoswiadczenia = new PunktyDoswiadczenia();
        punktyDoswiadczenia.setPunktyDoswiadczeniaObecne(100);
        PunktyDoswiadczeniaTools.sprawdzPD(punktyDoswiadczenia, 200, "Test");
    }

    @Test
    public void wykorzystajPD() {
        PunktyDoswiadczenia punktyDoswiadczenia = new PunktyDoswiadczenia();
        punktyDoswiadczenia.setPunktyDoswiadczeniaObecne(100);

        PunktyDoswiadczeniaTools.wykorzystajPD(punktyDoswiadczenia,70);
        Assert.assertEquals(30,punktyDoswiadczenia.getPunktyDoswiadczeniaObecne());
        Assert.assertEquals(70,punktyDoswiadczenia.getPunktyDoswiadczeniaRazem());
    }
}
