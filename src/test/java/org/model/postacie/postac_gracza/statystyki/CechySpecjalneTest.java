package org.model.postacie.postac_gracza.statystyki;

import org.model.cechy.CechyDrugorzedne;
import org.model.cechy.CechyGlowne;
import org.junit.Assert;
import org.junit.Test;

public class CechySpecjalneTest {

    @Test
    public void cechySpecjalneConstructor() {
        CechyGlowne cechyGlowne = new CechyGlowne(10,10,10,10,
                10,10,10,10);
        CechyDrugorzedne cechyDrugorzedne = new CechyDrugorzedne(0,0,0,0,
                0,0,0,0);
        CechySpecjalne cechySpecjalne = new CechySpecjalne(
                cechyGlowne,
                cechyGlowne,
                cechyDrugorzedne,
                cechyDrugorzedne
        );
        Assert.assertNotSame(cechyGlowne, cechySpecjalne.getCechyGlowneSchematRoznicowy());
        Assert.assertNotSame(cechyGlowne, cechySpecjalne.getCechyGlowneAktualne());
        Assert.assertNotSame(cechyDrugorzedne, cechySpecjalne.getCechyDrugorzedneSchematRoznicowy());
        Assert.assertNotSame(cechyDrugorzedne, cechySpecjalne.getCechyDrugorzedneSchematRoznicowy());
    }
}
