package org.model.postacie.postac_gracza;

import org.model.cechy.CechyDrugorzedne;
import org.model.cechy.CechyGlowne;
import org.model.data_files.CECHY_DRUGORZEDNE;
import org.model.data_files.CECHY_GLOWNE;
import org.model.profesje.Akolita;
import org.model.rasy.Rasa;
import org.junit.Assert;
import org.junit.Test;

public class UlepszanieCechTest {

//    @Test
//    public void UlepszCecheGlowna() {
//        PostacGracza postacGracza = new PostacGracza(
//                "Borubar",
//                new Rasa(
//                        "RasaTestowa",
//                        new CechyGlowne(
//                                1,2,3,4,
//                                5,6,7,8),
//                        new CechyDrugorzedne(
//                                1,2,3,4,
//                                5,6,7,8)),
//                new Akolita()
//        );
//        postacGracza.getPunktyDoswiadczenia().setPunktyDoswiadczeniaObecne(100);
//
//        UlepszenieCech.UlepszCecheGlowna(postacGracza, CECHY_GLOWNE.WALKA_WRECZ);
//        Assert.assertEquals(
//                6,
//                postacGracza.getCechySpecjalne().getCechyGlowneAktualne().getWalkaWrecz().getWartosc());
//
//        Assert.assertEquals(
//                0,
//                postacGracza.getCechySpecjalne().getCechyGlowneSchematRoznicowy().getWalkaWrecz().getWartosc());
//
//        Assert.assertEquals(0,postacGracza.getPunktyDoswiadczenia().getPunktyDoswiadczeniaObecne());
//        Assert.assertEquals(100, postacGracza.getPunktyDoswiadczenia().getPunktyDoswiadczeniaRazem());
//    }
//
//    @Test
//    public void UlepszCecheGlowna2() {
//        PostacGracza postacGracza = new PostacGracza(
//                "Borubar",
//                new Rasa(
//                        "RasaTestowa",
//                        new CechyGlowne(
//                                1,2,3,5,
//                                5,6,7,8),
//                        new CechyDrugorzedne(
//                                1,2,3,0,
//                                5,6,7,8)),
//                new Akolita()
//        );
//        postacGracza.getPunktyDoswiadczenia().setPunktyDoswiadczeniaObecne(100);
//
//        UlepszenieCech.UlepszCecheGlowna(postacGracza, CECHY_GLOWNE.ODPORNOSC);
//
//        Assert.assertEquals(
//                10,
//                postacGracza.getCechySpecjalne().getCechyGlowneAktualne().getOdpornosc().getWartosc());
//        Assert.assertEquals(
//                0,
//                postacGracza.getCechySpecjalne().getCechyGlowneSchematRoznicowy().getOdpornosc().getWartosc());
//        Assert.assertEquals(
//                1,
//                postacGracza.getCechySpecjalne().getCechyDrugorzedneAktualne().getWytrzymalosc().getWartosc());
//
//        Assert.assertEquals(0,postacGracza.getPunktyDoswiadczenia().getPunktyDoswiadczeniaObecne());
//        Assert.assertEquals(100, postacGracza.getPunktyDoswiadczenia().getPunktyDoswiadczeniaRazem());
//
//    }
//
//    @Test
//    public void UlepszCecheDrugorzedna() {
//        PostacGracza postacGracza = new PostacGracza(
//                "Borubar",
//                new Rasa(
//                        "RasaTestowa",
//                        new CechyGlowne(
//                                1,2,3,4,
//                                5,6,7,8),
//                        new CechyDrugorzedne(
//                                1,2,3,4,
//                                5,6,7,8)),
//                new Akolita()
//        );
//        postacGracza.getPunktyDoswiadczenia().setPunktyDoswiadczeniaObecne(100);
//
//        UlepszenieCech.UlepszCecheDrugorzedna(postacGracza, CECHY_DRUGORZEDNE.ZYWOTNOSC);
//        Assert.assertEquals(
//                3,
//                postacGracza.getCechySpecjalne().getCechyDrugorzedneAktualne().getZywotnosc().getWartosc());
//
//        Assert.assertEquals(
//                1,
//                postacGracza.getCechySpecjalne().getCechyDrugorzedneSchematRoznicowy().getZywotnosc().getWartosc());
//
//        Assert.assertEquals(0,postacGracza.getPunktyDoswiadczenia().getPunktyDoswiadczeniaObecne());
//        Assert.assertEquals(100, postacGracza.getPunktyDoswiadczenia().getPunktyDoswiadczeniaRazem());
//    }
}
