package org.model.rasy;

import org.model.cechy.CechyDrugorzedne;
import org.model.cechy.CechyGlowne;
import org.junit.Assert;
import org.junit.Test;

public class RasaTest {

    @Test
    public void rasaConstructor() {
        Rasa rasa = new Rasa(
                "rasa",
                new CechyGlowne(10,10,10,10,
                        10,10,10,10),
                new CechyDrugorzedne(0,0,0,0,
                        0,0,0,0)
        );

        Assert.assertEquals(new CechyDrugorzedne(0,0,1,1,
                0,0,0,0), rasa.getCechyDrugorzedne());
    }

    @Test
    public void tabelaZywotnosci() {
        Assert.assertEquals(10,Rasa.tabelaZywotnosci(1,10,20,30,40));
        Assert.assertEquals(10,Rasa.tabelaZywotnosci(3,10,20,30,40));
        Assert.assertEquals(20,Rasa.tabelaZywotnosci(4,10,20,30,40));
        Assert.assertEquals(20,Rasa.tabelaZywotnosci(6,10,20,30,40));
        Assert.assertEquals(30,Rasa.tabelaZywotnosci(7,10,20,30,40));
        Assert.assertEquals(30,Rasa.tabelaZywotnosci(9,10,20,30,40));
        Assert.assertEquals(40,Rasa.tabelaZywotnosci(10,10,20,30,40));
    }

    @Test
    public void tabelaPunktyPrzeznaczenia() {
        Assert.assertEquals(10,Rasa.tabelaPunktyPrzeznaczenia(1,10,20,30));
        Assert.assertEquals(10,Rasa.tabelaPunktyPrzeznaczenia(4,10,20,30));
        Assert.assertEquals(20,Rasa.tabelaPunktyPrzeznaczenia(5,10,20,30));
        Assert.assertEquals(20,Rasa.tabelaPunktyPrzeznaczenia(7,10,20,30));
        Assert.assertEquals(30,Rasa.tabelaPunktyPrzeznaczenia(8,10,20,30));
    }
}
